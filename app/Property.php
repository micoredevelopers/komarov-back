<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //One to Many
    public function options() {
        return $this->hasMany('App\Option');
    }
}
