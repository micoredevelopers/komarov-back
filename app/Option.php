<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //One To Many
    public function children() {
        //self::class - имя текущей модели
        return $this->hasMany(self::class, 'parent_id');
    }
     //Many To Many
     public function products(){
//        return $this->belongsToMany('App\Product');
        return $this->belongsToMany('App\Product');
    }
     //One to Many
    public function property() {
        return $this->belongsTo('App\Property');
    }

    //One to Many
    public function category() {
        return $this->belongsTo('App\Category');
    }
}
