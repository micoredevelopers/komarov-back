<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['text', 'user_id', 'product_id', 'status'];
     //One to Many
    public function product() {
        return $this->belongsTo('App\Product');
    }
     //One to many
    public function user() {
        return $this->belongsTo('App\User');
    }
}
