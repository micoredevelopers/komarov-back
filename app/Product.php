<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['code', 'name', 'description', 'content', 'images', 'state', 'making', 'category_id'];


    //One To Many
    public function category() {
        return $this->belongsTo('App\Category');
    }

    //One To Many
    public function comments(){
        return $this->hasMany('App\Comment');
    }
     //Get options
     public function options(){
        return $this->belongsToMany('App\Option')->withPivot('propertyId');
    }
    //One To Many
    public function addoptions() {
        return $this->hasMany('App\Addoption');
    }
    

}