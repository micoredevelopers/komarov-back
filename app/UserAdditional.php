<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdditional extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'phone', 'sex'
    ];
   
    //One to One
    public function user(){
        return $this->belongsTo('App\User');
    }
}
