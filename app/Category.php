<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   //One To Many
   public function children() {
    //self::class - имя текущей модели
    return $this->hasMany(self::class, 'parent_id');
    }

    //One To Many
    public function products() {
        return $this->hasMany('App\Product');
    }
    //One To Many
    public function order_history() {
        return $this->hasMany('App\OrderHistory');
    }
    //One to Many
    public function options() {
        return $this->hasMany('App\Option');
    }
    //One to Many
    public function addoptions() {
        return $this->hasMany('App\Addoption');
    }
}
