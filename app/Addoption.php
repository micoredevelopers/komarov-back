<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addoption extends Model
{
    protected $fillable = ['price', 'weight', 'size', 'product_id'];


    //One To Many
    public function product() {
        return $this->belongsTo('App\Product');
    }

//    //Many To Many
//    public function orders(){
//        return $this->belongsToMany('App\Order');
//    }
    //One to Many
    public function category() {
        return $this->belongsTo('App\Category');
    }
    

}