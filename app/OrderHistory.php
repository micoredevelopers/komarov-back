<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $fillable = ['order_id', 'code', 'name', 'description', 'content', 'images', 'state', 'making', 'category_id', 'sub_category', 'price', 'count', 'size', 'weight'];

    //One To Many
    public function order() {
        return $this->belongsTo('App\Opder');
    }
    //One To Many
    public function category() {
        return $this->belongsTo('App\Category');
    }
}
