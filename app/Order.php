<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['firstname', 'lastname', 'description', 'email', 'phone', 'sex', 'city', 'delivery', 'address', 'sum', 'state'];
//    //Many to Many
//    public function addoptions(){
//        return $this->belongsToMany('App\Addoption');
//    }

   // One to Many
    public function order_history(){
        return $this->hasMany('App\OrderHistory');
    }


     //One to Many
    public function user() {
        return $this->belongsTo('App\User');
    }
}
