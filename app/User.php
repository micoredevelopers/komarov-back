<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     //One to One
     public function user_additional(){
        return $this->hasOne('App\UserAdditional');
    }
    //One to Many
     public function comments()
     {
         return $this->hasMany('App\Comment');
     }
      //One to Many
     public function orders()
     {
         return $this->hasMany('App\Order');
     }
}
