<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        //проверка если логин не админ, отправлять на вторизацию
        if(!Auth::check() || ($user['name'] != 'admin')){
            return redirect()->route('adminLogin');
        }
        else return $next($request);
    }
}
