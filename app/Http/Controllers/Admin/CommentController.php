<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index()
    {
        //поиск
        if (Input::has('search')) {
            $comments =  Comment::whereHas('user', function ($query) {
                $query->whereHas('user_additional', function ($query) {
                    $search = Input::get('search');
                    $query->where('firstname','like', '%' . $search . '%')->orWhere('lastname', 'like', '%' . $search . '%');
                });
            })->orderBy('created_at', 'asc')->paginate(6);
            return view('admin.comments.index', [
                'comments' => $comments
            ]);
        }
        $comments =  Comment::with('user','product')->orderBy('created_at', 'desc')->paginate(6);
        return view('admin.comments.index', [
            'comments' => $comments
        ]);
    }
    // заблокировать коммент
    public function block(Comment $comment)
    {
        $comment->update(['status' => 'Запрещенный']);
        return redirect()->back();
    }
    // разрешить коммент
    public function access(Comment $comment)
    {
        $comment->update(['status' => 'Разрешенный']);
        return redirect()->back();
    }
}
