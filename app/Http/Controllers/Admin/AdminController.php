<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

   public function store(){

        return redirect('admin/product');

   }

   public function index() {

    return view('admin.setting', [
        'user' => Auth::user()
    ]);
   }


   public  function update(Request $request){

       //был ли пользователь аутентифицирован
       if(Auth::Check())
       {
           $request_data = $request->All();
           $validator = $this->admin_credential_rules($request_data);
           if($validator->fails())
           {
               return redirect()->back()->withErrors($validator);
           }
           else
           {
               $current_password = Auth::User()->password;
               if(Hash::check($request_data['current-password'], $current_password))
               {
                   $user_id = Auth::User()->id;
                   $obj_user = User::find($user_id);
                   $obj_user->password = Hash::make($request_data['password']);
                   $obj_user->save();
                   return redirect()->back()->with('success', ['Пароль успешно обновлен']);
               }
               else
               {
                   $error = array('current-password' => 'Пожалуйста, введите корректный текущий пароль');
                   return redirect()->back()->with('error' , $error);
               }
           }
       }
       else
       {
           return redirect()->to('/');
       }

   }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => 'Введите текущий пароль',
            'password.required' => 'Введите новый пароль',
            'password_confirmation.required' => 'Подтвердите новый пароль',
            'password.same:password' => 'Новый пароль и подтвержение пароля должно совпадать',
            'password_confirmation.same:password' => 'Новый пароль и подтвержение пароля должно совпадать',
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',
        ], $messages);

        return $validator;
    }
}
