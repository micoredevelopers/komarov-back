<?php

namespace App\Http\Controllers\Admin;

use App\Addoption;
use App\Category;
use App\Product;
use App\Property;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use FFMpeg;
use Illuminate\Support\Facades\Auth;
use Pbmedia\LaravelFFMpeg\MediaExporter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use function PHPSTORM_META\type;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // //поиск
        if (Input::has('search')) {
            $search = Input::get('search');



            $products = Product::with('category')->where('name', 'like', '%' . $search . '%')->orWhere('code', 'like', '%' . $search . '%')->paginate(6);
            return view('admin.products.index', [
                'products' => $products,
            ]);
        }
        $products = Product::with('category')->orderBy('created_at', 'desc')->paginate(6);
        return view('admin.products.index', [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_sizes_ring = [14, 14.5, 15, 15.5, 16, 16.2, 16.5, 17, 17.5, 17.8, 18, 18.5, 19, 19.5, 20, 20.5, 21, 21.5, 22, 22.5];
        $product_sizes_chain = [35, 40, 43, 45, 50, 55, 60, 65, 70, 75, 80, 85, 100, 130, 135, 140];


        $properties = Property::with('options')->get();

        return view('admin.products.create', [
            'product' => [],
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'properties' => $properties,
            'product_sizes_chain' => $product_sizes_chain,
            'product_sizes_ring' => $product_sizes_ring
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->input('properties'));
        $data = $request->all();
        $data['making'] = (int)$request['making'];

        $validator = Validator::make($data, [
            'code' => 'required|unique:products|max:120',
            'name' => "required|string|min:2",
            'category_id' => "required",
            'sub_category' => "sometimes|required",
            'weight.*' => "required",
            'images' => "required",
            'price.*' => "required",
//            'size.*' => "required",
            'making' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return redirect('admin/product/create')
                ->withErrors($validator)
                ->withInput();
        }

        //Проверка отправил ли пользователь изображение
        if ($request->hasFile('images')) {

            foreach ($request->images as $image) {
                if ($image->isValid()) {
                    $result_images[] = $this->addImage($image);
                }
            }
            $images_convert = json_encode($result_images);
            $data['images'] = $images_convert;
        }


        if ($request->sub_category) $data['category_id'] = $request->sub_category;
        else $data['category_id'] = $request->category_id;

        //создание товара
        $product = Product::create($data);

        //связь один ко многим с addoptions - price, weight, size
        if (isset($data['price'])) {

            foreach ($data['price'] as $key => $item) {
                $product->addoptions()->create([
                    'product_id' => $product->id,
                    'price' => (int)$item,
                    'weight' => (int)$data['weight'][$key],
                    'size' => isset($data['size'][$key]) ? (int)$data['size'][$key] : null
                ]);
            }

        }
        // связь многие ко многим с опциями второй параметр добавляет в pivot поле данные
        if ($request->input('properties')) {
            foreach($request->input('properties') as $key =>$item){
            $product->options()->attach($item, ['propertyId' => $key]);
                }
        }

        return redirect()->route('admin.product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product_sizes_ring = [14, 14.5, 15, 15.5, 16, 16.5, 17, 17.5, 17.8, 18, 18.5, 19, 19.5, 20, 20.5, 21, 21.5, 22, 22.5];
        $product_sizes_chain = [35, 40, 43, 45, 50, 55, 60, 65, 70, 75, 80, 85, 100, 130, 135, 140];

//        (!$product->category->parent_id) {$product->category->title
//											$product->category->where('id', $product->category->parent_id)->first()->title
        $pr_subcat = 0;
        $product->images = json_decode($product->images);
        $product->load('category','options', 'addoptions');

        //если родителя нет, значит это категория
        if (!$product->category->parent_id){
            $pr_cat = $product->category->id;
            $pr_subcat = 0;
        } else {
            $pr_subcat = $product->category->id;
            $pr_cat = $product->category->where('id', $product->category->parent_id)->first()->id;
        }
        $properties = Property::with('options')->get();
        //создание новости
        return view('admin.products.edit', [
            'product' => $product,
            'pr_cat' => $pr_cat,
            'pr_subcat' => $pr_subcat,
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'properties' => $properties,
//            'product_sizes' => $product_sizes,
            'product_sizes_chain' => $product_sizes_chain,
            'product_sizes_ring' => $product_sizes_ring
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
//        dd($request->input('properties'));

        if ($product->code == $request->code){
            $data = $request->except('code');
        }
        else {
            $data = $request->all();
        }
        $data['making'] = (int)$request['making'];
        $validator = Validator::make($data, [
            'code' => 'sometimes|required|unique:products|max:120',
            'name' => "required|string|min:2",
            'category_id' => "required",
            'sub_category' => "sometimes|required",
//            'images' => "sometimes|required",
            'weight.*' => "required",
            'price.*' => "required",
//            'size.*' => "required",
            'making' => 'required|numeric|min:1',
        ]);

        //проверка на изображение, если удалили старое и не загрузили новое - ошибка
        $validator->after(function ($validator) {
            $close_img = $validator->getData()['close_img'];
            $old_image = $validator->getData()['old_img'];
            //проверка если удалили изображение
            for ($i = 0; $i <= 3; $i++) {
                if ($close_img[$i]) {
                    $old_image[$i] = null;
                }
            }
            //вернет массив значений != 0
            $filtered = array_where($old_image, function ($value, $key) {
                return !is_null($value);
            });
            //если удалили все изображения и не добавили новое ошибка
            if (!isset($validator->getData()['images']) && (empty($filtered)) ) {
                    $validator->errors()->add('images', 'This field is required');
                }

        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $close_img = $request->close_img;
        $old_image = $request->old_img;

        //если пришло новое изображение вместо старого - старое удаляем, новое записываем в новую ячейку
        for ($i = 0; $i <= 3; $i++) {
            if (isset($request->images[$i]) && ($request->images[$i])->isValid()) {
                //удаление старого изображения
                if (!empty($old_image[$i])) {
                  Storage::disk('upload')->delete($old_image[$i]);
                    //удаляем превью
                    Storage::disk('upload')->delete(str_replace('.mp4', '.png', $old_image[$i]));
                    Storage::disk('upload')->delete(str_replace('.mov', '.png', $old_image[$i]));
                }
                $old_image[$i] = $this->addImage($request->images[$i]);

            }
            //если удалили старое изображение, а новое не пришло, удаляем его
            elseif ($close_img[$i]) {
                Storage::disk('upload')->delete($old_image[$i]);
                //удаляем превью
                Storage::disk('upload')->delete(str_replace('.mp4', '.png', $old_image[$i]));
                Storage::disk('upload')->delete(str_replace('.mov', '.png', $old_image[$i]));
                $old_image[$i] = null;

            }
        }

        $data_images = json_encode($old_image);

        if($data['sub_category']) {
            $data['category_id'] = $data['sub_category'];
        }

        //обновление товара
        $product->update($request->except('images', 'category_id'));

        //обновление категории и изображения в товаре
        $product->update(['images' => $data_images, 'category_id' => $data['category_id']]);


        // обновление опций
        $product->options()->detach();
        if ($request->input('properties')) :
            // attach() - присоединяет массив с категориями
            //$request->input('categories') - это id категорий, к которым относится новость
                foreach($request->input('properties') as $key =>$item){
                    $product->options()->attach($item, ['propertyId' => $key]);
            }
        endif;

        //связь один ко многим с addoptions - price, weight, size
        $product->addoptions()->delete();
        if (isset($request->price)) {
            foreach ($request->price as $key => $item) {
                $product->addoptions()->create([
                    'product_id' => $product->id,
                    'price' => (int)$item,
                    'weight' => (int)$request->weight[$key],
                    'size' => isset($data['size'][$key]) ? (int)$data['size'][$key] : null
                ]);
            }

        }
        return redirect()->route('admin.product.index');

    }


    public function destroy(Product $product)
    {

        //работа с изображениями если они есть
        if ($product->images) {
            $images = json_decode($product->images);
           // $path = Config::get('settings.product_path');
            foreach ($images as $image) {
                Storage::disk('upload')->delete($image);
                //удаляем превью
                Storage::disk('upload')->delete(str_replace('.mp4', '.png', $image));
                Storage::disk('upload')->delete(str_replace('.mov', '.png', $image));
            }
        }

//        //получаем список продуктов, которые нужно удалить, отсоединяем их от заказов
//        $del_addopts = $product->addoptions()->get();
//        foreach($del_addopts as $del_addopt) {
//            foreach($del_addopt->orders() as $order) {
//                dd($order);
//            }
//
//
//            // Пересчитыаем СУММУ , а потом удаляем продукт
//            //Обновляем заказы - сумма $order->update['sum', $sum]
//
//
//            $del_addopt->orders()->detach();
//        }
//

        //отсоединяем все связи с опциями
        $product->options()->detach();
        $product->addoptions()->delete();
        //удаляем экземпляр новости из базы
        $product->delete();

        return redirect()->route('admin.product.index');

    }




    // кнопка не в наличии
    public function absent(Product $product){
        ($product->state) ? $product->update(['state' => 0]) : $product->update(['state' => 1]);
        return redirect()->back();
    }



    public function addImage($image)
    {
        //$path = Config::get('settings.product_path');
          //  Storage::disk('public')->makeDirectory($path);
        $size_path = Config::get('settings.product_image');
        switch ($image->getMimeType()) {
            case 'image/jpeg':
                $str = str_random(8) . '.jpeg';
                // Get the filepath of the request file (.tmp) and append .jpg
                $requestImagePath = $image->getRealPath() . '.jpeg';
                // Modify the image using intervention
                $interventionImage = Image::make($image)->widen($size_path['width'])->encode('jpeg');
                // Save the intervention image over the request image
                $interventionImage->save($requestImagePath);
                //Send to storage
                Storage::disk('upload')->putFileAs('', new File($requestImagePath), $str);
                $result_image = $str;
                break;
            case 'image/jpg':
                $str = str_random(8) . '.jpg';
                $requestImagePath = $image->getRealPath() . '.jpg';
                $interventionImage = Image::make($image)->widen($size_path['width'])->encode('jpg');
                $interventionImage->save($requestImagePath);
                Storage::disk('upload')->putFileAs('', new File($requestImagePath), $str);
                $result_image = $str;
                break;
            case 'image/png':
                $str = str_random(8) . '.png';
                $requestImagePath = $image->getRealPath() . '.png';
                $interventionImage = Image::make($image)->widen($size_path['width'])->encode('png');
                $interventionImage->save($requestImagePath);
                Storage::disk('upload')->putFileAs('', new File($requestImagePath), $str);
                $result_image = $str;
                break;
            case 'video/mov':
                $str = str_random(8) ;
                Storage::disk('upload')->putFileAs('', new File($image), $str . '.mov');

                //делаем превью
                FFMpeg::fromDisk('upload')
                    ->open($str . '.mov')
                    ->getFrameFromSeconds(10)
                    ->export()
                    ->toDisk('upload')
                    ->save($str . '.png');
                $result_image = $str . '.mov';
                break;
            case 'video/mp4':
                $str = str_random(8);
                Storage::disk('upload')->putFileAs('', new File($image), $str . '.mp4');

                //делаем превью
                FFMpeg::fromDisk('upload')
                    ->open($str . '.mp4')
                    ->getFrameFromSeconds(10)
                    ->export()
                    ->toDisk('upload')
                    ->save($str . '.png');

                $result_image = $str . '.mp4';
                break;
            default:
                break;
        }
        return $result_image;
    }

}