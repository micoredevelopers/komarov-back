<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use NovaPoshta\Config;
use NovaPoshta\ApiModels\Address;
use NovaPoshta\MethodParameters\Address_getCities;
use NovaPoshta\MethodParameters\Address_getWarehouses;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Image;
use FFMpeg;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // //поиск
        if (Input::has('search')) {
            $search = Input::get('search');

            $orders = Order::with('order_history')->where('id', 'like', '%' . $search . '%')->orWhere('lastname', 'like', '%' . $search . '%')->paginate(6);
            return view('admin.orders.index', [
                'orders' => $orders,
            ]);
        }
        $orders = Order::with('order_history')->orderBy('created_at', 'desc')->paginate(6);

        return view('admin.orders.index', [
            'orders' => $orders,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {

        Config::setApiKey('ff43e4d939510bf5d965298a3d75bd12');
        Config::setFormat(Config::FORMAT_JSON);
        Config::setLanguage((Config::LANGUAGE_RU));


        $cities = Address::getCities();
        $list_city = $cities->data;


        $products = $order->order_history()->get();
          //dd($products[0]->id);
        return view('admin.orders.edit', [
            'order' => $order,
            'products' => $products,
            'list_city' => $list_city
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
//        dd($request->id_addoption);
        $data = $request->except(['user_id', 'city', 'sum']);

        $validator = $this->admin_credential_rules($data);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {


            //переводим код города в название
            $cities = new Address_getCities();
            $cities->setRef($request->city);
            $city = Address::getCities($cities)->data[0]->DescriptionRu;

            $sum = 0;
            //удаление товаров из заказа
            if (isset($request->del_prod)) {
                foreach ($request->del_prod as $key => $item) {
                    //если значение в поле 1, значит товар нужно удалить
                    if ($item) {
                        //удаляем изображение
                        Storage::disk('order')->delete($order->order_history[$key]->image);
                        $order->order_history[$key]->delete($request->id_addition[$key]);
                    }
                    //иначе считаем сумму
                    else $sum += $order->order_history[$key]->price * $order->order_history[$key]->count;
                }
            }
            $order->update($data);
            $order->update(['city' => $city]);
            $order->update(['sum' => $sum]);

            return redirect()->route('admin.order.index');
        }
    }


    public function destroy(Order $order)
    {
        //отсоединяем все связи с опциями
        $order->order_history()->delete();
        $order->delete();

        return redirect()->route('admin.order.index');

    }

    public function admin_credential_rules(array $data)
    {

        $validator = Validator::make($data, [
            'firstname' => "required|string|min:2",
            'lastname' => "required|string|min:2",
            'email' => 'string|email|max:255',
            'phone' => "numeric:min:10",
        ]);

        return $validator;
    }


    public function addImage($image)
    {
        $size_path = Config::get('settings.order_image');
        //если существует это изображение, достаем его
        if (Storage::disk('upload')->exists($image)) {
            Storage::disk('upload')->get($image);
            $str = str_random(8) . '.jpeg';
            // Get the filepath of the request file (.tmp) and append .jpg
            $requestImagePath = $image->getRealPath();
            // Modify the image using intervention
            $interventionImage = Image::make($image)->widen($size_path['width'])->encode('jpeg');
            // Save the intervention image over the request image
            $interventionImage->save($requestImagePath);
            //Send to storage
            Storage::disk('order')->putFileAs('', new File($requestImagePath), $str);

            return $str;
        } else
            return false;
    }

}
