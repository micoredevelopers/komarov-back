<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MyAuthController extends Controller
{
  //  protected $username = 'name';
    public function showLogin()
    {
        return view('admin.auth.login');
    }

    public function authenticate(Request $request)
    {
        $array = $request->all();

        //если пользователь активировал чекбокс
        // $remember = $request->has('remember');
        //выполняет аутентификацию пользователя
        //[] - 1) поле для поиска пользователя
        //$remember - второй аргуметнт функции, если активирован флаг
        if (Auth::attempt([
            'name' => $array['name'],
            'password' => $array['password'],
            ]))
        {
            //если результат true
            //intended() - перенаправляет на url, на который хотел попасть пользователь
            //если пути не существует передаем запасной путь
            return redirect()->intended('/admin/product');
        }
        //withInput() - сохраняет в сессию ранее внесенные данные, перечисляем какие поля сохраняем...без пароля
        //withErrors() - вывод ошибок
        return redirect()->back()
                            ->withInput($request->only('name'))
                            ->withErrors(['name' => 'Данные аутентификации не верны']);

    }


    public function logout(Request $request)
    {
    
        Auth::logout();
        return redirect()->route('index');
    }
}
