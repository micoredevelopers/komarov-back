<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\UserAdditional;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //поиск
        if (Input::has('search')) {
           $users =  User::whereHas('user_additional', function ($query) {
                $search = Input::get('search');

                $query->where('firstname', 'like', '%' . $search . '%')->orWhere('lastname', 'like', '%' . $search . '%');
            })->where('name', '<>', 'admin')->orderBy('created_at', 'desc')->paginate(6);
            return view('admin.users.index', [
                'users' => $users
            ]);
        }
        $users =  User::with('user_additional')->where('name', '<>', 'admin')->orderBy('created_at', 'desc')->paginate(6);
        return view('admin.users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        $user->user_additional()->delete();
        $user->comments()->delete();

        return redirect()->route('admin.user.index');
    }

    // заблокировать пользователя
    public function block(User $user)
    {
        ($user->status) ? $user->update(['status' => 0]) : $user->update(['status' => 1]);
        return redirect()->back();
    }
}
