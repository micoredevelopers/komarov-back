<?php

namespace App\Http\Controllers;

use App\Addoption;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Option;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;


class IndexController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', 0)->get();
        $popular_products = Addoption::with('product')->orderBy('price', 'desc')->limit(15)->get()->groupBy('product_id')->toArray();
        $new_products = Product::orderBy('created_at', 'desc')->limit(15)->get()->toArray();

        return view('index', [
            'categories' => $categories,
            'popular_products' => $popular_products,
            'new_products' => $new_products
        ]);
    }

    //отображение категорий, в скобках параметр который может прийти из get запросса
    public function category($slug, Request $request)
    {

        // находим категорию, которая пришла к нам из запроса
        $category = Category::where('slug', $slug)->first();
        $parent_cat = $category->parent_id;
        $cat_id = $category->id;
        (!$category->parent_id) ? $cats = $category->children()->pluck('id') : $cats = [];

        //список для фильтров
        (!$category->parent_id) ? $prods = Product::with('options', 'addoptions')->whereIn('category_id', $cats)->get() :
            $prods = $category->products()->with('options','addoptions')->get();

        //получаем список категорий
        (!$parent_cat) ?
            $category_list = $category :
            $category_list = Category::with('children')->where('id', $category->parent_id)->first();

        $filter_lists = [];
        // ФИЛЬТРАЦИЯ ДАННЫХ
        if( $request->except('page') ){
            $filter_properties = $request->except('_token', 'category', 'size', 'priceMin','priceMax', 'page');
            //для отображения уже нажатых флиьтров

                $filter_lists[] = $filter_properties;
                $filter_lists[] = $request->only('size', 'priceMin', 'priceMax');

//            dd($filter_lists[1]);
            $min = $request->priceMin;
            $max = $request->priceMax;
            $filter_sizes = ($request->size) ? $request->size : [];
            //массив options and properties
            $filter_opt = [];
            $filter_props = [];
            if (!empty($filter_properties)) {
                foreach ($filter_properties as $key => $filter_prop) {
                    $filter_props[] = $key;
                    foreach ($filter_prop as $item) {
                        array_push($filter_opt, $item);
                    }
                }
            }
             $f_props_select = array_unique($filter_props);
             $count_f_prop = count($f_props_select);

            $products = Product::when($filter_properties, function($query, $filter_properties) use ($filter_opt, $f_props_select, $count_f_prop){
                $query->whereHas('options' , function ($query) use ($filter_opt, $f_props_select, $filter_properties, $count_f_prop) {
                    //выборка по опциям только если они есть в фильтре
                        $query->whereIn('option_id', $filter_opt);
                        $query->whereIn('propertyId', $f_props_select);
                        $query->havingRaw('COUNT(option_id) = ?', [$count_f_prop]);
                             });
                         }
                         , function ($query) {
                                 $query->with('options');
                             })
                //выборка по цене и по размерам
                ->whereHas('addoptions', function ($query) use ($min, $max, $filter_sizes) {
                    $query->whereBetween('price', [$min, $max])
                        ->when($filter_sizes, function ($query, $filter_sizes) {
                            return $query->whereIn('size', $filter_sizes);
                        });
                })
                //выборка по категориям если это родитель то выбираем и подкатегории
                ->when($parent_cat, function ($query) use ($cat_id) {
                    return $query->where('category_id', $cat_id);
                }, function ($query) use ($cats) {
                    return $query->whereIn('category_id', $cats);
                })->get();
        }
        else {

            //если родительская категория
            if (!$category->parent_id) {
                //Выбираем все продукты из дочерних
                $products = Product::with('options')->whereIn('category_id', $cats)->get();
            } else {
                $products = $category->products()->with('options')->get();
            }
        }


        $sizes = [];
        $opt = [];

        //получаем набор опций и размеры, в которых есть продукты в данной категории
        foreach($prods as $product){
            foreach($product->options as $option) {
                $opt[] = $option->id;
                }
                //записываем только размеры отличные от 0
            foreach($product->addoptions as $addoption) {
                if ($addoption->size) $sizes[] = $addoption->size;
                }

        }
        $options = Option::with('property')->whereIn('id', $opt)->get();
        $property = [];
        foreach ($options as $option){
            $property[$option->property->id] = $option->property->name;
        }
        $request->getPathInfo();
        //Pagination
        $products = $this->PaginationProducts($products,14,$request);
        return view('category', [
            'category' => $category,
            'category_list' => $category_list,
            'products' => $products,
            'options' => $options,
            'properties' =>  array_unique($property),
            'sizes' => array_unique($sizes),
            'filter_lists' => $filter_lists
        ]);
    }

    public function PaginationProducts($products, $perPage, $request){

        $page = Input::get('page', 1); // Get the ?page=1 from the url
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(
            $products->forPage($page, $perPage)->values(),
            $products->count(),
            $perPage,
            $page,
            ['path' => $request->getRequestUri()]
        );
    }


    public function product($id)
    {
        $product = Product::where('id', $id)->first();
        $comments = $product->comments->load('user');

        return view('product', [
            'product' => $product,
            'comments' => $comments
        ]);
    }
}
