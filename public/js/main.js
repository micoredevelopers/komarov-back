//Инициализация слайдера и вызов функций при загрузке документа
$(document).ready(function () {
    var $carouselMain = $('.main-carousel');
    var $carouselNews = $('.products-news-carousel');
    $carouselMain.on('ready.flickity', function () {
        $('.bg_overlay').addClass('remove');
    });
    $carouselMain.flickity({
        cellAlign: 'center',
        contain: true,
        wrapAround: true,
        pageDots: false,
        arrowShape: {
            x0: 35,
            x1: 55,
            y1: 20,
            x2: 65,
            y2: 20,
            x3: 45
        }
    });
    $carouselNews.flickity({
        cellAlign: 'left',
        contain: true,
        wrapAround: true,
        pageDots: false,
        freeScroll: true,
        arrowShape: {
            x0: 35,
            x1: 55,
            y1: 25,
            x2: 70,
            y2: 25,
            x3: 50
        }
    });
    setTimeout(function () {
        $carouselMain.flickity('resize');
    }, 10);

    if ($(window).scrollTop() > 30) {
        $('header').addClass('active');
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() > 30) {
            $('header').addClass('active');
        } else if ($(window).scrollTop() < 30) {
            $('header').removeClass('active');
        }
    });

});


//Показ-скрытие подложки
function overlayShow() {
    $('.menu_overlay').addClass('onMenuOverShow');
}
function overlayHide() {
    $('.menu_overlay').removeClass('onMenuOverShow');
}
function overlayBodyShow() {
    $('.body_overlay').addClass('onOverShow');
}
function overlayBodyHide() {
    $('.body_overlay').removeClass('onOverShow');
}


function getActiveForm () {
    return $([...$('.form_container').has('.cart_block_body')].find((element, key) => $(element).css('display') === 'block'));
}

//Подсчет суммы в корзине
function countSum() {
    var $sumContainer = $('.products_cart_container .product_block');
    var $sumAll = $('.sum_container .sum_all span');
    // var sumInput = $('.product_price text');
    var sum = 0;
    for (i = 0; i < $sumContainer.length; i++) {
        var $sumEl = parseFloat($($('.product_price .text .cart_price')[i]).html());
        sum += $sumEl;
    }
    $sumAll.html(Math.round(sum));
    // sumInput.attr('value', sum);
}


//Подсчет суммы в подробной корзине
function countSumDetailed() {
    var $sumContainer = getActiveForm().find('.cart_block_body');

    var $sumAll = $('.sum_count .text span');
    // var sumInput = $('.product_price text');
    var sum = 0;
    for (var i = 0; i < $sumContainer.length; i++) {
        const $currentRowSpan =$( $sumContainer.get(i)).find($('.product_right_block .product_price .text span'));
        var $sumEl = parseFloat($($currentRowSpan).html());
        sum += $sumEl;

    }
    $sumAll.html(Math.round(sum));
    // sumInput.attr('value', sum);
    
}


//Открытие-закрытие меню мобилки
$('.hamb_menu').on('click', function () {
    $('.mobile_menu').addClass('onShow');
    $('.body_overlay').addClass('onOverShow');
    $('body').addClass('overHide');
    $('.body_overlay').css('z-index', '998');
});
$('.menu_close').on('click', function () {
    $('.mobile_menu').removeClass('onShow');
    $('.body_overlay').removeClass('onOverShow');
    $('body').removeClass('overHide');
    $('.body_overlay').css('z-index', '-1');
});


//Открытие подкатегории мобилки
$('.list_item.mobile').on('click', function (e) {
    if ($(e.target).hasClass('list_item')) {
        $(this).find($('.category_menu')).addClass('onCategoryShow');
        overlayShow();
    }
});


//Открытие подкатегорий на уст-вах выще мобилки
$('.list_item.mobile_higher').on('click', function () {
    const $rightBarMenu = $('.right_bar .category_menu');
    const $arrow = $(this).find('.arrow_container');
    $('.arrow_container').removeClass('rotate');
    if ($arrow.hasClass('rotate')) {
        $arrow.removeClass('rotate');
    } else {
        $arrow.addClass('rotate');
    }
    for (i = 0; i < $rightBarMenu.length; i++) {
        if ($(this).attr('id') === $($rightBarMenu[i]).attr('data-categ')) {
            $rightBarMenu.removeClass('categShow');
            if ($($rightBarMenu[i]).hasClass('categShow')) {
                $($rightBarMenu[i]).removeClass('categShow');
            } else {
                $($rightBarMenu[i]).addClass('categShow');
            }
        }
    }
});


//Возврат к категориям
$('.menu_back').on('click', function () {
    $('.category_menu').removeClass('onCategoryShow');
    overlayHide();
});


//Откритие посика в меню
$('.menu_search').on('click', function () {
    $('.menu_search_container').addClass('onSearchShow');
    $('.body_overlay').addClass('onOverShow');
    $('.body_overlay').css('z-index', '9998');
});


//Закрытие поиска в меню
$('.menu_search_back').on('click', function () {
    $('.menu_search_container').removeClass('onSearchShow');
    $('.body_overlay').css('z-index', '998');
});


//Показ меню подкатегорий на десктопе
$('.nav_container .list_item p').on('click', function () {
    $(this).parents('.list_item').find('.desktop_menu').addClass('onMenuDesktopShow');
    overlayBodyShow();
});


//Скрытие меню подкатегорий в десктопе
$('.desktop_menu .close_icon').on('click', function () {
    $(this).parents().find('.desktop_menu').removeClass('onMenuDesktopShow');
    $('.account_container .desktop_menu_body').addClass('active');
    $('.form_container').removeClass('active');
    $('.acc_click').removeClass('onDisabled');
    overlayBodyHide();
});


//Функции при нажатии на подложку
$('.body_overlay').on('click', function () {
    $('.desktop_menu').removeClass('onMenuDesktopShow');
    $('.account_container .desktop_menu_body').addClass('active');
    $('.form_container').removeClass('active');
    $('.acc_click').removeClass('onDisabled');
    $('.filters_menu').removeClass('filtersShow');
    $('.cart_menu').removeClass('cartShow');
    overlayBodyHide();
    $('body').removeClass('overHide');
    $('.body_overlay').removeClass('onOverHeader');
});


//Показ окна с авторизацией в меню
$('.acc_click').on('click', function () {
    $('.account_container .desktop_menu_body').addClass('active');
    $(this).addClass('onDisabled');
});


//Скрытие окна с авторизацией в меню
$('.auth a').on('click', function () {
    $('.account_container .desktop_menu_body').removeClass('active');
    $('.form_container').addClass('active');
});


//Показ меню с фильтрами
$('.filters_btn_container').on('click', function () {
    $('.filters_menu').addClass('filtersShow');
    $('body').addClass('overHide');
    overlayBodyShow();
    $('.body_overlay').addClass('onOverHeader');
});


//Скрытие меню с фильтрами
$('.close_filters').on('click', function () {
    $('.filters_menu').removeClass('filtersShow');
    $('body').removeClass('overHide');
    overlayBodyHide();
    $('.body_overlay').removeClass('onOverHeader');
});


//Добавление точки к checkbox'ам
$('.check_container input').on('click', function () {
    var $inputLabel = $(this).parent('.check_container');
    $inputLabel.toggleClass('active');
});


// //Инициализцаии ползунка в фильтрах
// $("#slider-range").slider({
//     range: true,
//     orientation: "horizontal",
//     min: 0,
//     max: 10000,
//     values: [100, 1500],
//     step: 10,
//
//     slide: function (event, ui) {
//         if (ui.values[0] == ui.values[1]) {
//             return false;
//         }
//         // $("#min_price").attr('value', ui.values[0])
//         // $("#max_price").attr('value', ui.values[1])
//     },
//     change: function (event, ui) {
//         $("#min_price").attr('value', ui.values[0]);
//         $("#max_price").attr('value', ui.values[1]);
//     }
//
// });
// $( "#slider-range" ).slider( "option", "min", 10 );

//Открытие корзины
$('.cart_container').click(function () {
    $('.cart_menu').addClass('cartShow');
    $('body').addClass('overHide');
    overlayBodyShow();
});


//Закрытие корзины
$('.cart_close').click(function () {
    $('.cart_menu').removeClass('cartShow');
    $('body').removeClass('overHide');
    overlayBodyHide();
    $('.account_container .desktop_menu_body').removeClass('active');
    $('.acc_click').removeClass('onDisabled');
    $('.desktop_menu').removeClass('onMenuDesktopShow');
});


//Удаление товара с корзины
$('.remove_btn span').click(function () {
    $(this).parents('.product_block').remove();
    countSum();
});


//Показ полного текста при нажатии на "Подробнее" (на главной)
$('.section_about_info .my_btn').on('click', function () {
    $('.text_info').addClass('onFullShow');
    $(this).remove();
});


//Добавление и инициализация инпут с числами
$(function () {
    $('.input_container').append('<div class="dec button"><span>-</span><img src="assets/images/cart/minus_-_anticon.png"></div>' + '<input class="input_number" type="number" min="1" max="100" step="1" value="1">' + '<div class="inc button"><span>+</span><img src="assets/images/cart/plus_-_anticon.png"></div>');
    $(".button").on("click", function () {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        var newVal = parseFloat(oldValue) + 1;
        var $productPrice = $(this).parents(".cart_block_body").find('.product_detailed .product_right_block .product_price .text span');
        if ($button.find('span').text() == "+") {
            newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 0) {
                newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0; 
            }
        }
        $button.parent().find("input").val(newVal);
        var productSum = parseFloat(Math.round($productPrice.attr('data-price')) * newVal);
        $productPrice.html(productSum);
        countSumDetailed();
        // console.log(countSumDetailed());
        
    });
    $('.input_container input').change(function () {
        var $input = $(this);
        var inputVal = parseFloat($input.val());
        var $productPrice = $(this).parents('.cart_block_body').find('.product_detailed .product_right_block .product_price .text span');
        var productSum = parseFloat(Math.round($productPrice.attr('data-price')) * inputVal);
        $productPrice.html(productSum);
        countSumDetailed();
    });
});


//Удаление твоара из подробной корзины
$('.product_right_block .product_remove .remove_btn').click(function () {
    $(this).parents('.cart_block_body').remove();
    countSumDetailed();
});


//Добавление и инициализация селекта с размерами
$(".custom-select").each(function () {
    var classes = $(this).attr("class"),
        id = $(this).attr("id"),
        name = $(this).attr("name");
    var template = '<div class="' + classes + '">';
    template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
    template += '<div class="custom-options">';
    $(this).find("option").each(function () {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
    });
    template += '</div></div>';

    $(this).wrap('<div class="custom-select-wrapper"></div>');
    $(this).hide();
    $(this).after(template);
});
$(".custom-option:first-of-type").hover(function () {
    $(this).parents(".custom-options").addClass("option-hover");
}, function () {
    $(this).parents(".custom-options").removeClass("option-hover");
});
$(".custom-select-trigger").on("click", function () {
    $('html').one('click', function () {
        $(".custom-select").removeClass("opened");
    });
    $(this).parents(".custom-select").toggleClass("opened");
    event.stopPropagation();
});
$(".custom-option").on("click", function () {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});





//Вызов функций
countSumDetailed();
countSum();
