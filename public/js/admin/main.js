// ВАЛИДАЦИЯ НА ЛОГИНЕ
$('.btn_custom').on('click', function () {
    var login_form = document.getElementById('login_form'),
        password_form = document.getElementById('pass_form'),
        btn_form = document.getElementsByClassName('.btn_custom');

    if ((login_form.value) !== "" && (password_form.value) !== "") {
        login_form.style.border = '1px solid #bbbaba';
        password_form.style.border = '1px solid #bbbaba';
    } else {
        login_form.style.border = '1px solid #d0021b';
        password_form.style.border = '1px solid #d0021b';
    }
});

let POSSIBLE_PRODUCT_SIZES = [
    // RINGS
    [14, 14.5, 15, 15.5, 16, 16.2, 16.5, 17, 17.5, 17.8, 18, 18.5, 19, 19.5, 20, 20.5, 21, 21.5, 22, 22.5],
    // CHAIN
    [35, 40, 43, 45, 50, 55, 60, 65, 70, 75, 80, 85, 100, 130, 135, 140]
];
// ФУНКЦИЯ ДЛЯ РАЗМЕРОВ В ТОВАРЕ
let PRODUCT_SIZES = [14, 14.5, 15, 15.5, 16, 16.2, 16.5, 17, 17.5, 17.8, 18, 18.5, 19, 19.5, 20, 20.5, 21, 21.5, 22, 22.5];

// ВСПЛЫВАЮЩИЕ МЕНЮ РЕДАКТИРОВАНИЯ В ТАБЛИЦЕ
$('.order_button').on('click', function () {
    var toggleBlock = $('.detailed_block');
    if ($(toggleBlock).hasClass('active') && $(this).find(toggleBlock).hasClass('active')) {
        $(toggleBlock).removeClass('active');
    } else {
        $(toggleBlock).removeClass('active');
        $(this).find(toggleBlock).addClass('active');
    }
});


// ЗАКРЫТИЕ МЕНЮ ПРИ НАЖАТИИ НА ЛЮБОЕ МЕСТО
$('.table_container').on('click', function (e) {
    if (e.target.className !== 'detailed_block' && e.target.className !== 'btn_show') {
        $('.detailed_block').removeClass('active');
    }
});


// РАСЧЕТ ОБЩЕЙ СУММЫ ВСЕХ ЗАКАЗОВ
function countSum() {
    var sumContainer = $('.product_info');

    var sumAll = $('.sum_all .text .sum span');
    var sumInput = $('.sum_all input');
    var sum = 0;
    for (i = 0; i < sumContainer.length; i++) {
        var sumEl = parseFloat($($('.product_price .text p span')[i]).html());
        sum += sumEl;
    }
    sumAll.html(sum);
    sumInput.attr('value', sum);
}

function onCategoryChange() {
    const $productBlock = $($('.psw_container').first());
    const selectedValue = $('#select-main').val();

    if (selectedValue == 1 || selectedValue == 2) {
        PRODUCT_SIZES = POSSIBLE_PRODUCT_SIZES[(selectedValue - 1)];

        $productBlock.empty().append(getProdContainer())

        $('#add').css({
            display: 'block'
        })
    } else {
        $productBlock.empty();
        $productBlock.empty().append(getProdContainer())
        $productBlock.find('.delete_psw').remove();
        $productBlock.find('.styled-size').remove();
        $('#add').css({
            display: 'none'
        })

    }

    EnabledOptions();
}

function prepareOpts() {
    const $productBlock = $($('.psw_container').first());
    const selectedValue = $('#select-main').val();

    if (selectedValue != 1 && selectedValue != 2) {
        $productBlock.find('.delete_psw').remove();
        $productBlock.find('.styled-size').remove();
        $('#add').css({
            display: 'none'
        })
    }
}

function EnabledOptions() {
    const selectedValue = $('#select-main').val();

    if (selectedValue) {
        $('#select-sub').attr('disabled', false);
        $('.sub-select').removeClass('active');
    }

    const options = [];

    $('#select-sub option').each((key, option) => {
        const $option = $(option);
        $option.attr('selected', false);
        $option.css('display', 'none');

        if (selectedValue === $option.attr('data-id')) {
            $option.css('display', 'block');
            options.push($option);
        }

    });

    if (!options[0]) {
        $('#select-sub #empty-sub-cat').css({
            display: 'block'
        });
        $('#select-sub #empty-sub-cat').attr('selected', 'selected');
        $('#select-sub #empty-sub-cat').prop("selected", true);
    } else {
        $($(options).first()[0]).attr('selected', true);
        $($(options).first()[0]).prop("selected", true);
    }



    // CHANGE PRODUCT TYPE ON CATEGORY CHANGE

    const $prodType = $('#prop-5');
    const $prodTypes = $prodType.find('option');
    $prodTypes.css({
        display: 'none'
    });

    $prodType.attr('disabled', true);
    $prodType.parent().addClass('active');

    $prodTypes.each((key, option) => {
        const $option = $(option)
        $option.attr('selected', false);
        $option.prop("selected", false);

        if (selectedValue == $option.attr('data-id') || selectedValue == 1) {

            $option.css({
                display: 'block'
            })

            $prodType.parent().removeClass('active');
            $prodType.attr('disabled', false);
        }
    })
    $prodTypes.first().attr('selected', true);
}

// CHANGE SAMPLE ON METAL CHANGE
$('#prop-2').on('change', function () {
    const $metalVal = $(this).find('option:selected').attr('value')
    // METAL SAMPLES 
    const $metalSample = $('#prop-3');
    const $metalSamplesOpts = $metalSample.find('option');
    let isFirstEntry = true

    $metalSamplesOpts.each((key, opt) => {
        const $opt = $(opt);
        $opt.attr('selected', false);
        $opt.css({
            display: 'none',
        })

        if ($opt.attr('data-id') === $metalVal) {
            $opt.css({
                display: 'block',
            })
            if (isFirstEntry) {
                $opt.attr('selected', true);
                $opt.prop('selected', true);
            }
            isFirstEntry = false;
        }
    })
})

// ОТКЛЮЧЕНИЕ СЕЛЕКТА ПОДКАТЕГОРИЙ
$('#select-main').change(onCategoryChange);

// ПРОВРЕКА, ЕСЛИ ЗНАЧЕНИЕ ВВЕДЕНО В КАТЕГОРИЮ ВКЛЮЧАЕТСЯ СЕЛЕКТ ПОДКАТЕГОРИЙ
function categCheck() {
    if ($('#select-sub').attr('disabled', true)) {
        $('.sub-select').addClass('active');
    }
}

// ОГРАНИЧЕНИЕ МАКСИМАЛЬНО ЗАГРУЖЕНЫХ ФОТОГРАФИЙ
function imgLoad() {
    $("input[type='file']").change(function () {
        if (parseInt($(this).get(0).files.length) > 4) {
            alert("You can only upload a maximum of 4 files");
        }
    });
}


// ФУНКЦИЯ ПОЛУЧЕНИЯ РАЗМЕРОВ
function getProductSizes() {
    return PRODUCT_SIZES.map((size, index) => {
        return `<option id="${(index)}" value="${(size)}">${size}</option>`;
    })
}

// ФУНКЦИЯ ФОРМИРОВАНИЯ КОНТЕЙНЕРА С ИНПУТАМИ
function getProdContainer() {
    return `
        <div class="input_container">
            <input id="product-price" name="price[]" type="text" placeholder="150 грн.">
            <input id="product-weight" name="weight[]" type="text" placeholder="15 грамм">
            <div class="styled-select styled-size">
                <select name="size[]" class="styled select-size">
                    <option id="0" value="0" selected>Размер</option>
                    ${getProductSizes()}
                </select>
            </div>
            <a class="delete_psw" href="#"><img src="../../images/admin/x_icon.png" alt=""></a>
        </div>
    `
}

// ФУНКЦИЯ ИЗМЕНЕНИЯ РАЗМЕРА
function preparePossibleOptions() {
    const choosedValues = $('.psw_container')
        .find('option:selected')
        .map((key, val) => $(val).attr('value')).toArray();

    $('.psw_container').find('option').each((key, el) => {
        if (choosedValues.includes($(el).val()) && $(el).attr('value') !== '0') {
            $(el).css({
                display: 'none'
            });
        } else {
            if ($(el).attr('value') !== '0') {
                $(el).css({
                    display: 'inline'
                });
            }
        }
    });
}

// ФУНКЦИЯ ДОБАВЛЕНИЯ КОНТЕЙНЕРА С РАЗМЕРАМИ ЦЕНАМИ И ВЕСОМ
function addProductInfo() {

    const $container = $(getProdContainer());

    $container.find('.delete_psw').on('click', function () {
        $(this).parent()[0].remove();
        preparePossibleOptions();
    });


    $container.find('select').change(preparePossibleOptions);

    $container.find('#product-weight').change(addedText('грамм'));

    $container.find('#product-price').change(addedText('грн.'));

    $($('.psw_container')[0]).append($container);

    preparePossibleOptions();
}

// ФУНКЦИЯ ДЛЯ РАЗМЕРОВ В ТОВАРЕ
// ФУНКЦИЯ ДЛЯ УДАЛЕНИЯ КАРТИНКИ И ВИДЕО
function removeImg() {
    // $(this).parent().find('input').val('');
    // $(this).parent().find('label').find('img').attr('src', '');
    $(this).parent().find('label').find('img').remove();
    $(this).parent().find('.close_img').val('1');

}

// ФУНКЦИЯ ДЛЯ УДАЛЕНИЯ КАРТИНКИ И ВИДЕО


// ФУНКЦИЯ ДЛЯ ДОБАВЛЕНИЕ ФОТО-ВИДЕО

$('.image-upload').change(function (event) {
    var file = event.target.files[0];
    var fileReader = new FileReader();
    if (file.type.match('image')) {
        fileReader.onload = function () {
            var img = document.createElement('img');
            img.src = fileReader.result;
            $(event.target).parent().find('label').find('img').remove();
            $(event.target).parent().find('label').append(img);
        };
        fileReader.readAsDataURL(file);
    } else {
        fileReader.onload = function () {

            var blob = new Blob([fileReader.result], {
                type: file.type
            });
            var url = URL.createObjectURL(blob);
            var video = document.createElement('video');

            var snapImage = function () {
                var canvas = document.createElement('canvas');
                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;
                canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                var image = canvas.toDataURL();
                var success = image.length > 100000;
                if (success) {
                    var img = document.createElement('img');
                    img.src = image;
                    // $(event.target).parent().find('label').append(img);
                    $(event.target).parent().find('label').find('img').remove();
                    $(event.target).parent().find('label').append(img);
                    URL.revokeObjectURL(url);
                }
                return success;
            };
            var timeupdate = function () {
                if (snapImage()) {
                    video.removeEventListener('timeupdate', timeupdate);
                    video.pause();
                }
            };
            video.addEventListener('loadeddata', function () {
                if (snapImage()) {
                    video.removeEventListener('timeupdate', timeupdate);
                }
            });
            video.addEventListener('timeupdate', timeupdate);
            video.preload = 'metadata';
            video.src = url;

            // Load video in Safari / IE11
            video.muted = true;
            video.playsInline = true;
            video.play();
        };
        fileReader.readAsArrayBuffer(file);
    }
});

// ФУНКЦИЯ ДЛЯ ДОБАВЛЕНИЕ ФОТО-ВИДЕО


// ФУНКЦИЯ ДОБАВЛЕНИЯ ТЕКСТА В ИНПУТ С ЦИФРАМИ
function addedText(text) {
    return function () {
        var inputValue = parseInt($(this).val());
        $(this).val((inputValue || '') + (inputValue ? (' ' + text) : ''));
    }
}

// ФУНКЦИЯ ДОБАВЛЕНИЯ ТЕКСТА В ИНПУТ С ЦИФРАМИ


// ФУНКЦИИ ПО СОБЫТИЯМ
$('#add').on('click', addProductInfo);

$('.images_block .image_container a').on('click', removeImg);

$('#day-count').change(addedText('дней'));

$('#delete_btn').on('click', function () {
    $(this).parents('tr').remove();
});

$('.product_price .btn_container a').on('click', function () {
    var isTrue = confirm("Вы уверены, что хотите удалить?");
    if (!isTrue) {
        e.preventDefault();
    } else {
        if ($(this).parents('.list_container .parent_product_info').get(0) === $(this).parents('.list_container').find('.parent_product_info').get(0)) {
            alert("Товар нельзя удалить!")
        } else {
            $(this).parents('.parent_product_info').find('.del_prod').attr('value', 1);
            $(this).parents('.product_info').remove();
        }
        countSum();
    }
});


$('.delete').on('click', function (e) {
    var isTrue = confirm("Вы уверены, что хотите удалить?");
    if (!isTrue) {
        e.preventDefault();
    }
});

// ВЫЗОВ ФУНКЦИЙ

if ($('.psw_container .input_container').length === 0) {

    addProductInfo();

}

$('.delete_psw').on('click', function () {
    $(this).parent()[0].remove();
});



//НОВАЯ ПОЧТА
$('#city_id').on('change', function () {

    $.post({
        url: $(this).attr('data-action'),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id: $(this).val(),
            order_id: $(this).data('order_id')
        }
    }).then(function (data) {
        //select с отделениями новой почты
        const res = data.data.map((office, index) => {
            return `<option id="${(index)}" value="${(office)}">${office}</option>`;
            // if (office == data.order) {
            //     return `<option id="${(index)}" value="${(office)}" selected="">${office}</option>`;
            // } else {
            //     return `<option id="${(index)}" value="${(office)}">${office}</option>`;
            // }
        });
        $('#address_id').append(res)
    })
});

$(document).ready(function () {
    EnabledOptions();
});

preparePossibleOptions();
prepareOpts();

imgLoad();
categCheck();
countSum();