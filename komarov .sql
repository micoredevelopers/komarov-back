-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 03 2018 г., 12:36
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `komarov`
--

-- --------------------------------------------------------

--
-- Структура таблицы `addoptions`
--

CREATE TABLE `addoptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double(8,2) DEFAULT NULL,
  `size` double(8,2) DEFAULT NULL,
  `weight` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `addoptions`
--

INSERT INTO `addoptions` (`id`, `product_id`, `price`, `size`, `weight`, `created_at`, `updated_at`) VALUES
(225, 43, 10000.00, 0.00, 15.00, '2018-08-31 08:00:33', '2018-08-31 08:00:33'),
(226, 42, 455.00, 15.00, 54.00, '2018-08-31 08:01:00', '2018-08-31 08:01:00'),
(249, 41, 1212.00, 15.00, 121.00, '2018-08-31 09:53:40', '2018-08-31 09:53:40'),
(250, 40, 1212.00, 14.00, 121.00, '2018-08-31 09:53:52', '2018-08-31 09:53:52'),
(255, 45, 34.00, 15.00, 34.00, '2018-09-03 04:23:58', '2018-09-03 04:23:58'),
(256, 39, 45.00, 0.00, 45.00, '2018-09-03 04:24:26', '2018-09-03 04:24:26'),
(257, 38, 444.00, 0.00, 44.00, '2018-09-03 04:24:42', '2018-09-03 04:24:42'),
(258, 37, 454.00, 0.00, 454.00, '2018-09-03 04:24:55', '2018-09-03 04:24:55'),
(259, 34, 1000.00, 16.00, 14.00, '2018-09-03 04:25:10', '2018-09-03 04:25:10'),
(260, 33, 33333.00, 15.00, 22.00, '2018-09-03 04:25:23', '2018-09-03 04:25:23'),
(261, 32, 34.00, 0.00, 34.00, '2018-09-03 04:25:37', '2018-09-03 04:25:37'),
(262, 31, 11111.00, 0.00, 11.00, '2018-09-03 04:25:59', '2018-09-03 04:25:59'),
(263, 30, 12.00, 18.00, 12.00, '2018-09-03 04:26:14', '2018-09-03 04:26:14'),
(264, 29, 3453.00, 15.00, 123.00, '2018-09-03 04:26:37', '2018-09-03 04:26:37'),
(265, 29, 10000.00, 20.00, 100.00, '2018-09-03 04:26:38', '2018-09-03 04:26:38'),
(266, 27, 10006.00, 0.00, 666.00, '2018-09-03 04:26:55', '2018-09-03 04:26:55'),
(267, 44, 44444.00, 15.00, 44.00, '2018-09-03 05:03:47', '2018-09-03 05:03:47');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `image`, `content`, `parent_id`, `position`, `created_at`, `updated_at`) VALUES
(1, 'Бриллианты', 'for-diamonds', 'cat.jpg', 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 1, '2018-08-07 21:00:00', NULL),
(2, 'Серьги', 'for-earrings', 'cat.jpg', 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 2, '2018-08-07 21:00:00', NULL),
(3, 'Кольца', 'for-ring', 'cat.jpg', 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 3, '2018-08-07 21:00:00', NULL),
(4, 'Цепочки', 'for-chain', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 4, '2018-08-07 21:00:00', NULL),
(5, 'Подвески', 'for-pendant', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 5, '2018-08-07 21:00:00', NULL),
(6, 'Крестики', 'for-crosses', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 6, '2018-08-07 21:00:00', NULL),
(7, 'Бижутерия', 'for-bijouterie', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 7, '2018-08-07 21:00:00', NULL),
(8, 'Булавки', 'for-pin', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 0, 8, '2018-08-07 21:00:00', NULL),
(9, 'Кольца', 'diamond-rings', 'cat.jpg', 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 1, 1, '2018-08-07 21:00:00', NULL),
(10, 'Серьги', 'diamond-earrings', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 1, 2, '2018-08-07 21:00:00', NULL),
(11, 'Подвески', 'diamond-pendants', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 1, 3, '2018-08-07 21:00:00', NULL),
(12, 'Обручальные кольца', 'diamond-wedding-rings', NULL, 'Stock up on men\'s clearance gear and take advantage of signature Nike items on sale. Featuring                                                 sneakers, clothing.', 1, 4, '2018-08-07 21:00:00', NULL),
(13, 'Колье', 'diamond-necklace', NULL, '', 1, 5, '2018-08-07 21:00:00', NULL),
(14, 'Крестики', 'diamond-crosses', NULL, '', 1, 6, '2018-08-07 21:00:00', NULL),
(15, 'С коньячными бриллиантами', 'diamond-with-cognac-diamonds', NULL, '', 1, 7, '2018-08-07 21:00:00', NULL),
(16, 'С чёрными бриллиантами', 'diamond-with-black-diamonds', NULL, '', 1, 8, '2018-08-07 21:00:00', NULL),
(17, 'С фианитами', 'earrings-with-cubic-zirconia', 'cat.jpg', '', 2, 1, '2018-08-07 21:00:00', NULL),
(18, 'С полудрагоценными камнями', 'earrings-with-semiprecious-stones', NULL, '', 2, 2, '2018-08-07 21:00:00', NULL),
(19, 'С жемчугом', 'earrings-with-pearls', NULL, '', 2, 3, '2018-08-07 21:00:00', NULL),
(20, 'Пусеты', 'earrings-pouches', NULL, '', 2, 4, '2018-08-07 21:00:00', NULL),
(21, 'Серьги-подвески', 'earrings-earring-pendants', NULL, '', 2, 5, '2018-08-07 21:00:00', NULL),
(22, 'Без вставки', 'earrings-without-insertion', NULL, '', 2, 6, '2018-08-07 21:00:00', NULL),
(23, 'Эксклюзивы', 'earrings-exclusives', NULL, '', 2, 7, '2018-08-07 21:00:00', NULL),
(38, 'С фианитами', 'ring-with-cubic-zirconia', 'cat.jpg', '', 3, 1, '2018-08-07 21:00:00', NULL),
(39, 'С полудрагоценными камнями', 'ring-with-semiprecious-stones', NULL, '', 3, 2, '2018-08-07 21:00:00', NULL),
(40, 'Обручальные кольца', 'ring-wedding-rings', NULL, '', 3, 3, '2018-08-07 21:00:00', NULL),
(41, 'Помолвочные кольца', 'ring-engagement-rings', NULL, '', 3, 4, '2018-08-07 21:00:00', NULL),
(42, 'С жемчугом', 'ring-with-pearls', NULL, '', 3, 5, '2018-08-07 21:00:00', NULL),
(43, 'Печатки', 'ring-eggs', NULL, '', 3, 6, '2018-08-07 21:00:00', NULL),
(44, 'Эксклюзивы', 'ring-exclusives', NULL, '', 3, 7, '2018-08-07 21:00:00', NULL),
(45, 'Золотые', 'chain-gold', NULL, '', 4, 1, '2018-08-07 21:00:00', NULL),
(46, 'Серебрянные', 'chain-silver', NULL, '', 4, 2, '2018-08-07 21:00:00', NULL),
(47, 'Колье', 'chain-necklace', NULL, '', 4, 3, '2018-08-07 21:00:00', NULL),
(48, 'Ювелирные шнурки', 'chain-jewelry-shoelaces', NULL, '', 4, 4, '2018-08-07 21:00:00', NULL),
(49, 'С фианитами', 'pendant-with-cubic-zirconia', NULL, '', 5, 1, '2018-08-07 21:00:00', NULL),
(50, 'С полудрагоценными камнями', 'pendant-with-semiprecious-stones', NULL, '', 5, 2, '2018-08-07 21:00:00', NULL),
(51, 'Крестики', 'pendant-сrosses', NULL, '', 5, 3, '2018-08-07 21:00:00', NULL),
(52, 'Иконки', 'pendant-icons', NULL, '', 5, 4, '2018-08-07 21:00:00', NULL),
(53, 'Буквы', 'pendant-letters', NULL, '', 5, 5, '2018-08-07 21:00:00', NULL),
(54, 'Знаки зодиака', 'pendant-zodiac-signs', NULL, '', 5, 6, '2018-08-07 21:00:00', NULL),
(55, 'Эксклюзивы', 'pendant-exclusives', NULL, '', 5, 7, '2018-08-07 21:00:00', NULL),
(56, 'Цепочки', 'bijouterie-chains', NULL, '', 7, 1, '2018-08-07 21:00:00', NULL),
(57, 'Колье', 'bijouterie-necklace', NULL, '', 7, 2, '2018-08-07 21:00:00', NULL),
(58, 'Браслеты', 'bijouterie-bracelets', NULL, '', 7, 3, '2018-08-07 21:00:00', NULL),
(59, 'Кольца', 'bijouterie-rings', NULL, '', 7, 4, '2018-08-07 21:00:00', NULL),
(60, 'Серьги', 'bijouterie-earrings', NULL, '', 7, 5, '2018-08-07 21:00:00', NULL),
(61, 'Кулоны', 'bijouterie-pendants', NULL, '', 7, 6, '2018-08-07 21:00:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `text`, `user_id`, `product_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Новый коммент2', 13, 29, 'Разрешенный', '2018-08-20 21:00:00', '2018-08-28 07:59:03'),
(3, 'Новый коммент5', 14, 31, 'Новый', '2018-08-20 21:00:00', NULL),
(5, 'Новый коммент4', 13, 32, 'Новый', '2018-08-20 21:00:00', NULL),
(6, 'Новый коммент4', 13, 32, 'Новый', '2018-08-20 21:00:00', NULL),
(7, 'Новый коммент4', 13, 32, 'Новый', '2018-08-20 21:00:00', NULL),
(8, 'Новый коммент5', 14, 31, 'Разрешенный', '2018-08-20 21:00:00', '2018-08-21 06:40:43'),
(9, 'Новый коммент2', 13, 29, 'Новый', '2018-08-20 21:00:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(46, '2014_10_12_000000_create_users_table', 1),
(47, '2014_10_12_100000_create_password_resets_table', 1),
(48, '2018_08_08_100546_create_categories_table', 1),
(49, '2018_08_08_111823_create_products_table', 1),
(50, '2018_08_08_135743_create_user_additionals_table', 1),
(51, '2018_08_08_140651_create_orders_table', 1),
(52, '2018_08_08_140730_create_comments_table', 1),
(53, '2018_08_09_081531_create_order_product_table', 1),
(54, '2018_08_10_115302_create_options_table', 1),
(55, '2018_08_10_115729_create_properties_table', 1),
(56, '2018_08_10_115947_create_option_product_table', 1),
(57, '2018_08_10_121742_create_addoptions_table', 1),
(58, '2018_08_17_070223_create_addoption_order_table', 1),
(59, '2018_08_31_072237_create_order_histories_table', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `property_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `name`, `property_id`, `parent_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'мужчине', 1, NULL, NULL, NULL, NULL),
(2, 'женщине', 1, NULL, NULL, NULL, NULL),
(3, 'ребенку', 1, NULL, NULL, NULL, NULL),
(4, 'золото', 2, NULL, NULL, NULL, NULL),
(5, 'серебро', 2, NULL, NULL, NULL, NULL),
(6, '585', 3, 4, NULL, NULL, NULL),
(7, '375', 3, 4, NULL, NULL, NULL),
(8, '925', 3, 5, NULL, NULL, NULL),
(9, 'белый', 4, NULL, NULL, NULL, NULL),
(10, 'комбинированный', 4, NULL, NULL, NULL, NULL),
(11, 'красный', 4, NULL, NULL, NULL, NULL),
(13, 'лимонная позолота', 4, NULL, NULL, NULL, NULL),
(14, 'лимонный', 4, NULL, NULL, NULL, NULL),
(20, 'безразмерные', 5, NULL, 3, NULL, NULL),
(21, 'наборные', 5, NULL, 3, NULL, NULL),
(22, 'обручальные', 5, NULL, 3, NULL, NULL),
(23, 'помолвочные', 5, NULL, 3, NULL, NULL),
(24, 'фаланговые', 5, NULL, 3, NULL, NULL),
(25, 'двойные пуссеты', 5, NULL, 2, NULL, NULL),
(26, 'каффы', 5, NULL, 2, NULL, NULL),
(27, 'конго', 5, NULL, 2, NULL, NULL),
(28, 'одиночные пуссеты', 5, NULL, 2, NULL, NULL),
(29, 'пуссеты', 5, NULL, 2, NULL, NULL),
(30, 'серьги-подвески', 5, NULL, 2, NULL, NULL),
(31, 'серьги-протяжки\r\n', 5, NULL, 2, NULL, NULL),
(32, 'колье', 5, NULL, 4, NULL, NULL),
(33, 'цепочки с подвеской', 5, NULL, 4, NULL, NULL),
(34, 'шнурки', 5, NULL, 4, NULL, NULL),
(35, 'буквы', 5, NULL, 5, NULL, NULL),
(36, 'крестики', 5, NULL, 5, NULL, NULL),
(37, 'ладанки', 5, NULL, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `option_product`
--

CREATE TABLE `option_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `option_product`
--

INSERT INTO `option_product` (`id`, `option_id`, `product_id`) VALUES
(274, 1, 42),
(275, 4, 42),
(276, 7, 42),
(277, 10, 42),
(350, 1, 45),
(351, 4, 45),
(352, 6, 45),
(353, 2, 38),
(354, 2, 27),
(355, 5, 27),
(356, 7, 27),
(357, 10, 27),
(358, 2, 44),
(359, 5, 44),
(360, 8, 44),
(361, 11, 44);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sum` double NOT NULL,
  `state` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `firstname`, `lastname`, `description`, `email`, `phone`, `sex`, `city`, `delivery`, `address`, `sum`, `state`, `created_at`, `updated_at`) VALUES
(2, NULL, 'Екатерина', 'Петрова', NULL, 'test@ukr.net', '55555555', '1', 'Одесса', 'Новая почта', 'Отделение №1: Киевское шоссе (ран. Ленинградское шоссе), 27', 100, 'Новый', '2018-08-16 21:00:00', '2018-08-31 06:42:46'),
(3, NULL, 'Екатерина', 'Петрова', 'erfergerg', 'test@ukr.net', '45456', '1', 'Одесса', 'Новая почта', 'Отделение №9 (до 30 кг): ул. Сегедская, 18', 173, 'Оплачен', '2018-08-16 21:00:00', '2018-08-31 06:55:38'),
(4, NULL, 'Екатерина', 'Петрова', NULL, 'test@ukr.net', 'fgbfg556', '1', 'Одесса', 'Новая почта', 'влопрвлоп', 0, 'Новый', '2018-08-16 21:00:00', '2018-08-17 07:03:29'),
(5, NULL, 'Екатерина', 'Петрова', NULL, 'test@ukr.net', 'fgbfg556', '1', 'Одесса', 'Новая почта', 'влопрвлоп', 0, 'Новый', '2018-08-16 21:00:00', '2018-08-17 07:03:29'),
(6, NULL, 'Екатерина', 'Петрова', NULL, 'test@ukr.net', 'fgbfg556', '1', 'Одесса', 'Новая почта', 'влопрвлоп', 0, 'Новый', '2018-08-16 21:00:00', '2018-08-17 07:03:29'),
(7, NULL, 'Екатерина', 'Петрова', NULL, 'test@ukr.net', 'fgbfg556', '1', 'Одесса', 'Новая почта', 'влопрвлоп', 0, 'Новый', '2018-08-16 21:00:00', '2018-08-17 07:03:29'),
(8, NULL, 'Екатерина', 'Петрова', NULL, 'test@ukr.net', 'fgbfg556', '1', 'Одесса', 'Новая почта', 'влопрвлоп', 0, 'Новый', '2018-08-16 21:00:00', '2018-08-17 07:03:29');

-- --------------------------------------------------------

--
-- Структура таблицы `order_histories`
--

CREATE TABLE `order_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `making` int(11) DEFAULT NULL,
  `category_id` tinyint(11) NOT NULL,
  `price` double NOT NULL,
  `count` tinyint(4) NOT NULL,
  `size` double(8,2) DEFAULT NULL,
  `weight` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `order_histories`
--

INSERT INTO `order_histories` (`id`, `order_id`, `code`, `name`, `description`, `content`, `image`, `making`, `category_id`, `price`, `count`, `size`, `weight`, `created_at`, `updated_at`) VALUES
(7, 2, '111', 'ertrt', NULL, NULL, 'MfWwNirs2.jpeg', NULL, 2, 100, 2, 15.00, 1.00, NULL, NULL),
(15, 3, '111', 'ertrt', NULL, NULL, 'MfWwNirs1.jpeg', NULL, 2, 50, 1, 15.00, 1.00, NULL, NULL),
(17, 3, '111', 'ertrt', NULL, NULL, 'MfWwNirs.jpeg', NULL, 2, 123, 1, 15.00, 1.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `making` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `category_id` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `description`, `content`, `images`, `making`, `state`, `category_id`, `created_at`, `updated_at`) VALUES
(27, '2', 'qqqq', NULL, 'rtrtrt', '[\"TZBTLV5L.jpeg\",null,null,null]', 2, 0, 13, '2018-08-16 03:50:11', '2018-09-03 04:26:55'),
(29, '1111', 'gfg', NULL, NULL, '[null,\"MfWwNirs.jpeg\",null,null]', 1, 0, 9, '2018-08-16 07:32:15', '2018-09-03 04:26:37'),
(30, '5', 'rr', NULL, NULL, '[null,null,\"rDwmm6cd.jpeg\",null]', 5, 0, 13, '2018-08-16 08:27:59', '2018-09-03 04:26:14'),
(31, '66', 'rty', NULL, NULL, '[null,\"iHIcKyV4.jpeg\",null,null]', 6, 0, 16, '2018-08-16 08:28:17', '2018-09-03 04:25:59'),
(32, '655', 'rtty', NULL, NULL, '[null,null,\"uqikaU7P.jpeg\",null]', 55, 0, 15, '2018-08-16 08:28:39', '2018-09-03 04:25:37'),
(33, '66665', 'yyyy', NULL, NULL, '[null,\"03bDDXR4.jpeg\",null,null]', 66, 0, 9, '2018-08-16 08:29:00', '2018-09-03 04:25:23'),
(34, '4456', 'rttryrt', NULL, NULL, '[\"93roA6bR.jpeg\",null,null,null]', 6666, 0, 9, '2018-08-16 08:29:20', '2018-09-03 04:25:10'),
(37, 'fgndfgn', 'fghgh', NULL, NULL, '[null,null,null,\"IX4645rp.jpeg\"]', 66, 0, 10, '2018-08-16 08:30:37', '2018-09-03 04:24:55'),
(38, '6rthdfgh', 'fgh', NULL, NULL, '[null,null,\"BtFVHtvR.jpeg\",null]', 55, 0, 14, '2018-08-16 08:31:00', '2018-09-03 04:24:42'),
(39, 'cvndgn', 'fgfgb', NULL, NULL, '[\"kZOrP9jB.jpeg\",null,null,null]', 6, 0, 11, '2018-08-16 08:31:11', '2018-09-03 04:24:26'),
(40, '5rstgsrtgh', 'tghfgh', NULL, NULL, '[\"tptoItw8.mp4\",null,null,null]', 66, 0, 10, '2018-08-16 08:31:27', '2018-08-31 09:53:52'),
(41, 'hdgj', 'thty', NULL, NULL, '[\"Zgl5X0N2.jpeg\",\"mdC4uASF.mp4\",\"bUzpV3pc.jpeg\",\"e90sg2CW.jpeg\"]', 6, 1, 9, '2018-08-16 08:31:52', '2018-08-31 09:53:40'),
(42, '56546', 'gsdfgdsfgdsfg', 'dfgdfg', '4dtfghfg', '[\"vKjDwlct.jpeg\",null,null,null]', 6, 0, 12, '2018-08-29 08:27:03', '2018-08-31 08:01:00'),
(43, 'h66', 'fhfgh', NULL, NULL, '[\"uGioHXbQ.jpeg\",null,null,null]', 55, 1, 11, '2018-08-29 08:27:37', '2018-08-31 08:00:33'),
(44, '456456456', 'золото товар', 'srg', 'sfdg', '[\"Yrpkb1eH.jpeg\",\"tWKRoSK5.jpeg\",null,null]', 65, 1, 12, '2018-08-29 08:28:08', '2018-09-03 05:03:47'),
(45, '111111', 'КАТЕГОРИЯ', NULL, NULL, '[\"aDRoDyt4.jpeg\",null,null,null]', 5, 1, 9, '2018-08-31 08:29:58', '2018-09-03 04:24:03');

-- --------------------------------------------------------

--
-- Структура таблицы `properties`
--

CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `properties`
--

INSERT INTO `properties` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'для кого', 'for-whom', NULL, NULL),
(2, 'металл', 'metal', NULL, NULL),
(3, 'проба', 'sample', NULL, NULL),
(4, 'цвет', 'color', NULL, NULL),
(5, 'тип изделия', 'type', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$x2wUOwSPDg5n1iVNXgevEuJjyzvKRIZzI/mvCiy1vUe2TDusrBxAS', 1, 'P8mf0xdSx149MPn32iLtCKYPKPXPnj9lHq35Zy0SC5bKkpwMyezGm6jgTCmu', '2018-08-13 04:17:52', '2018-08-21 06:41:08'),
(13, 'test3', 'Test@mail.ru3', '$2y$10$yJEfP2wbbajlbXJrY71lMuahsnTAI5oaKs8OU3vT6Bkrnb1.g1sc6', 1, 'NIJcTpf0ciavXrOp87cptdktBu6gOBbIGY19piwRz0I1a0UwgUcWDkMGEkS9', '2018-08-20 11:53:25', '2018-08-20 11:53:25'),
(14, 'test4', 'Test4@mail.ru', '$2y$10$aKw9qfaRGlssiyDSCoJYiOIptj0UUJDxxWgO5Ben0hHhx7ZH301BK', 1, 'iIAkRTgPnVxbz3U07KmwMZ46b5m5uXJqnwAKKjVPwtcVsopBmf2VyAgKVhEx', '2018-08-20 11:53:43', '2018-08-20 11:53:43'),
(15, 'test5', 'Test5@mail.ru', '$2y$10$uhTuQDwvPjLRdxHr4WbumeTHlQ8YUmA9xRvdVsYFM47XWzdxW93dK', 1, 'oxEA3FoN7pL6zpmD3prMdzprigehAJ79BJ9h0jIgPaoFW5fClqPDDKgRYqiz', '2018-08-20 11:53:56', '2018-08-20 11:53:56'),
(16, 'test6', 'Test@mail.ru6', '$2y$10$PpVQyF5doxBL7QCL9Mi4N./9l7nB/miCshB2vEYjl8EfXqmCbrCgO', 1, 'UlbIsbfZaIZHdVSpnOBXA430PoOClsc1N4LHgY21hCBxhrpYkoMoBbmWrekq', '2018-08-20 11:54:13', '2018-08-20 11:54:13'),
(17, 'test7', 'Test7@mail.ru', '$2y$10$HMNkCrSWd6BI3XTHUQeSNuBeZ5iOG76UAlhFDvB8nBYNu6JpedY5q', 1, '1q5HCFfNfKmcMBEhITTqngVwBWqxTWM4kkNGPbhpuBl09YX5mk33xS3nW79E', '2018-08-20 11:54:26', '2018-08-21 07:02:21');

-- --------------------------------------------------------

--
-- Структура таблицы `user_additionals`
--

CREATE TABLE `user_additionals` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_additionals`
--

INSERT INTO `user_additionals` (`id`, `user_id`, `firstname`, `lastname`, `phone`, `sex`, `created_at`, `updated_at`) VALUES
(1, 1, 'Екатерина', 'Петрова', '1111111111', 'Женщина', NULL, NULL),
(2, 13, 'Глеб', 'Петрова', NULL, NULL, NULL, NULL),
(3, 14, 'Виталий', 'Батькович', NULL, NULL, NULL, NULL),
(4, 15, 'Глеб', 'Георгиевич', NULL, NULL, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `addoptions`
--
ALTER TABLE `addoptions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `option_product`
--
ALTER TABLE `option_product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_histories`
--
ALTER TABLE `order_histories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_code_unique` (`code`);

--
-- Индексы таблицы `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `user_additionals`
--
ALTER TABLE `user_additionals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `addoptions`
--
ALTER TABLE `addoptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT для таблицы `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT для таблицы `option_product`
--
ALTER TABLE `option_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `order_histories`
--
ALTER TABLE `order_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `user_additionals`
--
ALTER TABLE `user_additionals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
