@extends('layouts.app')

@section('title', $category->title )

@section('wrap-class')
        <div id="wrapper-products-list">
@endsection

@section('content')
                <main>
                    <section class="section_products_list">
                        <div class="container">
                            <div class="section_title">
                                <h1 class="category_title">{{ $category_list->title }} @if ($category_list->title != $category->title) / {{ $category->title }} @endif</h1>
                                <div class="filters_btn_container">
                                    <span class="inner_text">Фильтры</span>
                                    <div class="filter_img">
                                            <img src="{{ asset('images/main/images/main/images/products-list/filter-button-image.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="sub_categories_list">
                                <div class="row m-0">
                                    @if ($category_list->children->count())
                                        @foreach($category_list->children as $sub_category)
                                    <div class="col-xl-3 col-6">
                                        <a href="{{ url("/category/$sub_category->slug")}}">
                                            <div class="sub_category_block">
                                                <div class="inner_image">
                                                    @if (isset($sub_category->image))
                                                        <img src="{{ asset('images/category' . '/' . $sub_category->image) }} " alt="">
                                                    @else
                                                        <img src="{{ asset('images/main/images/main/mages/sub-category-image.png') }}" alt="">
                                                    @endif
                                                </div>
                                                <div class="inner_text">
                                                    <p class="text">{{ $sub_category->title }}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                            {{--First 2 item products for desktop and mobile--}}
                            <div class="product_about mobile d-block d-xl-none">
                                <div class="row m-0">
                                    <div class="col-6">
                                        <div class="product_about_image">
                                            <img src="{{ asset('images/category' . '/' . $category->image) }} " alt="">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="product_about_text">
                                            <p class="text">{{ $category->content }}</p>
                                        </div>
                                    </div>

                                    @foreach ($products->slice(0, 2) as $product)
                                        @php $images = isset($product->images) ? json_decode($product->images) : '';
                                        @endphp
                                        <div class="col-6">
                                            <a href="#">
                                                <div class="product_block first_product">
                                                    <div class="product_image">
                                                        {{--Перебор images и ставим авой первое которое есть--}}
                                                            @foreach($images as $image)
                                                                @isset($image)
                                                                    @if (ends_with($image, '.mp4'))
                                                                        <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $image) ) }}" alt="">
                                                                    @elseif (ends_with($image, '.mov'))
                                                                        <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $image) ) }}" alt="">
                                                                    @else
                                                                        <img src=" {{ Storage::disk('upload')->url($image ) }}" alt="">
                                                                    @endif
                                                                    @break
                                                                @endisset
                                                            @endforeach
                                                    </div>
                                                    <div class="product_info">
                                                        <div class="product_text name">
                                                            <p class="text">{{ $product->name }}</p>
                                                        </div>
                                                        <div class="product_text price">
                                                            <p class="text">@if (isset($product->addoptions[0]->price)) {{ $product->addoptions[0]->price }}@endif грн</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="product_about desktop d-none d-xl-block">
                                <div class="row m-0 ">
                                    <div class="col-6 left_container">
                                        <div class="product_about_image">
                                            <img src="{{ asset('images/category' . '/' . $category->image) }} " alt="">
                                        </div>
                                    </div>
                                    <div class="col-6 right_container">
                                        <div class="row m-0">
                                            <div class="col-12 p-0">
                                                <div class="product_about_text">
                                                    <p class="text">{{ $category->content }}</p>
                                                </div>
                                            </div>
                                            @foreach ($products->slice(0, 2) as $product)
                                                @php $images = isset($product->images) ? json_decode($product->images) : '';
                                                @endphp
                                            <div class="col-6 pl-0 pr-3">
                                                <a href="#">
                                                    <div class="product_block first_product">
                                                        <div class="product_image">
                                                                {{--Перебор images и ставим авой первое которое есть--}}
                                                                    @foreach($images as $image)
                                                                        @isset($image)
                                                                            {{--если это видео и есть превью показываем его--}}
                                                                            @if (ends_with($image, '.mp4'))
                                                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $image) ) }}" alt="">
                                                                            @elseif (ends_with($image, '.mov'))
                                                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $image) ) }}" alt="">
                                                                            @else
                                                                                <img src=" {{ Storage::disk('upload')->url($image ) }}" alt="">
                                                                            @endif
                                                                            @break
                                                                        @endisset
                                                                    @endforeach

                                                        </div>
                                                        <div class="product_info">
                                                            <div class="product_text name">
                                                                <p class="text">{{ $product->name }}</p>
                                                            </div>
                                                            <div class="product_text price">
                                                                <p class="text">@if (isset($product->addoptions[0]->price)) {{ $product->addoptions[0]->price }}@endif грн</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            @endforeach
                                    </div>
                                </div>
                                </div>
                            </div>
                            {{--Product list--}}
                            <div class="products_container">
                                <div class="row m-0">
                                    @foreach ($products->slice(2) as $product)
                                        @php $images = isset($product->images) ? json_decode($product->images) : '';
                                        @endphp
                                        <div class="col-xl-3 col-6 pl-0 pr-3">
                                            <a href="#">
                                                <div class="product_block first_product first_product">
                                                    <div class="product_image">
                                                        {{--Перебор images и ставим авой первое которое есть--}}
                                                            @foreach($images as $image)
                                                                @isset($image)
                                                                    {{--если это видео и есть превью показываем его--}}
                                                                    @if (ends_with($image, '.mp4'))
                                                                        <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $image) ) }}" alt="">
                                                                    @elseif (ends_with($image, '.mov'))
                                                                        <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $image) ) }}" alt="">
                                                                    @else
                                                                        <img src=" {{ Storage::disk('upload')->url($image ) }}" alt="">
                                                                    @endif
                                                                    @break
                                                                @endisset
                                                            @endforeach
                                                    </div>
                                                    <div class="product_info">
                                                        <div class="product_text name">
                                                            <p class="text">{{ $product->name }}</p>
                                                        </div>
                                                        <div class="product_text price">
                                                            <p class="text">@if (isset($product->addoptions[0]->price)) {{ $product->addoptions[0]->price }}@endif грн</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            {{--Pagination--}}
                            <div class="pagination_nav">
                                    <div class="pagination_container">
                                        @if($products->lastPage() > 1)
                                            {{--next Page qrrow--}}
                                            @if($products->currentPage() !== 1)
                                                <div class="arrow_container arrow_prev">
                                                    <a href="{{ $products->url(($products->currentPage() - 1 )) }}">
                                                        <img src="{{ asset('images/main/images/products-list/pagination-arrow-left.png') }}" alt="">
                                                    </a>
                                                </div>
                                            @endif
                                            <ul class="pag_list">
                                                @for($i = 1; $i <= $products->lastPage(); $i++)
                                                    @if( $i == $products->currentPage())
                                                        <li class="pag_item active"><a class="page_numb" >{{ $i }}</a></li>
                                                    @else
                                                        @if ($i == 1)
                                                            <li class="pag_item first"><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
                                                        @endif

                                                        @if ($i == ($products->currentPage() - 1) && ($i !== 1 ))
                                                                @if(($products->currentPage() - 2) !== 1)
                                                                    <li class="pag_item dots"><span class="page_dots">...</span></li>
                                                                @endif
                                                            <li class="pag_item"><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
                                                        @endif

                                                        @if ( $i == ($products->currentPage() + 1) && ( $i !== $products->lastPage() ) )
                                                            <li class="pag_item mr-0" ><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
                                                                @if(($products->currentPage() + 2) !== $products->lastPage())
                                                                    <li class="pag_item dots"><span class="page_dots">...</span></li>
                                                                @endif
                                                        @endif
                                                        @if ($i == $products->lastPage())
                                                            <li class="pag_item last"><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
                                                        @endif

                                                    @endif
                                                @endfor
                                            </ul>
                                            {{--prev page arrow--}}
                                            @if($products->currentPage() !== $products->lastPage())
                                                <div class="arrow_container arrow_next">
                                                    <a href="{{ $products->url(($products->currentPage() + 1 )) }}">
                                                        <img src="{{ asset('images/main/images/products-list/pagintaion-arrow-right.png') }}" alt="">
                                                    </a>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </section>

                    <div class="filters_menu">
                        <div class="filters_container">
                            <div class="filters_header d-flex d-xl-none">
                                <div class="logo">
                                    <img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
                                </div>
                                <div class="close_filters">
                                    <img src="{{ asset('images/main/images/close-icon.png') }}" alt="">
                                </div>
                            </div>
                            <form action="{{ route('filter', ['slug' => $category->slug]) }}" method="get">
                                @include('filter-form')
                            </form>
                        </div>
                    </div>

                </main>

@endsection

