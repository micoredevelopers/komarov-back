<div class="filter_body">
    <div class="row m-0">
        {{--@php   dump($filter_lists) @endphp--}}
        @if( !empty($properties))
            @foreach($properties as $prop_id => $property)
                <div class="col-md-4 col-6">
                    <div class="filter_block">
                        <h1 class="title">{{ $property }}</h1>
                        <div class="inputs_container">


                            <div class="form-group">
                                @foreach($options as $opt_id => $option)
                                    @if ($property == $option->property->name)
                                <label class="check_container
                                     @if (isset($filter_lists[0][$prop_id]) && (in_array($option->id, $filter_lists[0][$prop_id])) ) active @endif">
                                    {{ $option->name }}
                                    <input type="checkbox" name="{{$option->property->id}}[]" value="{{ $option->id }}"
                                           @if (isset($filter_lists[0][$prop_id]) && (in_array($option->id, $filter_lists[0][$prop_id])) ) checked @endif>
                                    <span class="checkmark"></span>
                                </label>
                                    @endif
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endisset

        @if ($sizes)
        <div class="col-md-4 col-sm-6 col-12">
            <div class="filter_block">
                <h1 class="title">Размер</h1>
                <div class="inputs_container sizes">
                    <div class="inputs_row">
                        @foreach($sizes as $size)
                        <div class="form-group">
                            <label class="check_container
                            @if (isset($filter_lists[1]['size']) && (in_array($size, $filter_lists[1]['size'])) ) active @endif">
                                {{ $size }}
                                <input type="checkbox" name="size[]" value="{{ $size }}"
                                       @if (isset($filter_lists[1]['size']) && (in_array($size, $filter_lists[1]['size'])) ) checked @endif>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
            @endif

        <div class="col-xl-5 col-lg-6 col-12">
            <div class="filter_block price">
                <h1 class="title">Цена</h1>
                <div class="inputs_container">
                    <div class="form-group">
                        <div id="slider-range" class="price-filter-range"></div>
                        <div class="bottom_inputs">
                            <input value="400" name="priceMin" type="text" min=0 max="9900" oninput="validity.valid||(value='0');" id="min_price" class="price-range-field" />
                            <input value="1000" name ="priceMax" type="text" min=0 max="10000" oninput="validity.valid||(value='10000');" id="max_price"
                                   class="price-range-field" />
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-7 col-lg-6 col-12">
            <div class="btn_row">
                <div class="btn_container">
                    <a href="{{ route('filter', ['slug' => $category->slug]) }}" class="my_btn reset">Сбросить</a>
                </div>
                <div class="btn_container">
                    <button class="my_btn black" type="submit">Применить</button>
                </div>
            </div>
        </div>




    </div>
</div>