<header>
	<div class="header_mobile d-block d-xl-none">
		<div class="container">
			<div class="row m-0">
				<div class="col-12 p-0">
					<div class="header_container">
						<div class="hamb_menu">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<a href="{{ route('index') }}"><div class="logo">
							<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
						</div></a>
						<div class="cart_container">
							<a href="#">
								<span class="goods_count">5</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header_desktop d-none d-xl-block">
		<div class="container-fluid">
			<div class="row m-0">
				<div class="col-12 p-0">
					<div class="header_container">
						<a href="{{ route('index') }}"><div class="logo">
							<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
							</div></a>
						<div class="nav_container">
							<ul class="nav_list">
								@include('layouts.top_menu', ['categories' => $categories])
							</ul>
							<ul class="nav_list">
								<li class="list_item">
									<p>Поиск</p>
									<div class="desktop_menu menu_search_container d-none d-xl-block">
										<div class="container">
											<div class="desktop_menu_header">
												<div class="logo"><img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt=""></div>
												<div class="close_icon"><img src="{{ asset('images/main/images/close-icon.png') }}" alt=""></div>
											</div>
											<div class="search_body">
												<div class="row m-0">
													<div class="col-xl-4 offset-xl-1 p-0">
														<form class="group_form" action="#">
															<input class="search_input" name="search" type="text" placeholder="Поиск...">
															<button class="search_btn" type="submit"><img src="{{ asset('images/main/images/arrow-search.png') }}" alt=""></button>
														</form>
													</div>
													<div class="col-xl-6 offset-xl-1 p-0">
														<div class="samples_container">
															<div class="title">
																<h1>Примеры поиска</h1>
															</div>
															<div class="text">
																<p>Золотые кольца</p>
																<p>Серебренные Крестики</p>
																<p>Обручальные кольца</p>
																<p>Вы отправляете в мой город/страну?</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="list_item">
									<p>Помощь</p>
									<div class="desktop_menu category_menu d-none d-xl-block">
										<div class="container">
											<div class="desktop_menu_header">
												<div class="logo"><img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt=""></div>
												<div class="close_icon"><img src="{{ asset('images/main/images/close-icon.png') }}" alt=""></div>
											</div>
											<div class="category_container">
												<div class="category_body">
													<div class="freq_questions">
														<div class="row m-0">
															<div class="col-xl-3 offset-xl-1 p-0">
																<ul class="list">
																	<div class="list_title">
																		<h1>Контакты</h1>
																	</div>
																	<li class="list_item">
																		<a href="#">+38 (096) 123 45 67</a>
																	</li>
																	<li class="list_item">
																		<a href="#">+38 (096) 123 45 67</a>
																	</li>
																	<li class="list_item">
																		<a href="#">komarov@gmail.com</a>
																	</li>
																</ul>
															</div>
															<div class="col-xl-4 p-0">
																<ul class="list">
																	<div class="list_title">
																		<h1>Общая информация</h1>
																	</div>
																	<li class="list_item">
																		<a href="#">О магазине</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Доставка и оплата</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Доставка и оплата</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Возврат и обмен</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Руководство по размеру</a>
																	</li>
																</ul>
															</div>
															<div class="col-xl-4 p-0">
																<ul class="list">
																	<div class="list_title">
																		<h1>Часто задаваемые вопросы</h1>
																	</div>
																	<li class="list_item">
																		<a href="#">Мне нужна учётная запись для покупки?</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Сколько стоит доставка?</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Как мне вернуть или обменять?</a>
																	</li>
																	<li class="list_item">
																		<a href="#">Вы отправляете в мой город/страну?</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="list_item">
									<p class="acc_click">Аккаунт</p>
									<div class="desktop_menu account_container d-none d-xl-block">
										<div class="container">
											<div class="desktop_menu_body">
												<div class="account_info">
													<p class="text">Создайте учетную запись или войдите в систему, чтобы просматривать ваши заказы, редактировать свою личную информацию и добавлять товары в желаемое.</p>
												</div>
												<div class="bottom_submit">
													<div class="auth">
														<a href="javascript:void[0]">Авторизоваться</a>
													</div>
													<div class="btn_create">
														<a href="#" class="custom_btn">Создать аккаунт</a>
													</div>
												</div>
											</div>
											<form action="#" class="form_container">
												<div class="form-group">
													<label for="#">
														<p class="text">Email адрес*</p>
													</label>
													<input type="mail" placeholder="@gmail.com">
												</div>
												<div class="form-group last">
													<label for="#">
														<p class="text">Пароль*</p>
													</label>
													<input type="password">
												</div>
												<div class="bottom_submit">
													<div class="pass_forgot">
														<a href="#">Я забыл свой пароль</a>
													</div>
													<div class="btn_container">
														<button type="submit" class="custom_btn">Войти</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</li>
								<li class="list_item">
									<div class="cart_container">
										<a href="#">
											<span class="goods_count">5</span>
										</a>
									</div>
								</li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

{{--mobile--}}
<div class="mobile_menu d-block d-sm-none">
	<div class="menu_container">
		<div class="menu_header">
			<div class="menu_close">
				<img src="{{ asset('images/main/images/close-icon.png') }}" alt="">
			</div>
			<div class="menu_logo">
				<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
			</div>
			<div class="menu_search">
				<img src="{{ asset('images/main/images/search-icon.png') }}" alt="">
			</div>
		</div>
		<div class="menu_body body">
			<ul class="list_nav">
				@include('layouts.top_menu_mobile', ['categories' => $categories])
			</ul>
			<ul class="list_nav list_footer">
				<li class="list_item mobile">
					<span class="category_name">Помощь</span>
					<div class="arrow_container">
						<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">
					</div>
					<div class="category_menu">
						<div class="category_container">
							<div class="category_header">
								<div class="menu_back">
									<img src="{{ asset('images/main/images/category-back-arrow.png') }}" alt="">
								</div>
								<div class="menu_logo">
									<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
								</div>
								<div class="menu_search hide">
									<img src="{{ asset('images/main/images/search-icon.png') }}" alt="">
								</div>
							</div>
							<div class="category_body">
								<div class="freq_questions">
									<ul class="list">
										<div class="list_title">
											<h1>Часто задаваемые вопросы</h1>
										</div>
										<li class="list_item">
											<a href="#">Мне нужна учётная запись для покупки?</a>
										</li>
										<li class="list_item">
											<a href="#">Сколько стоит доставка?</a>
										</li>
										<li class="list_item">
											<a href="#">Как мне вернуть или обменять?</a>
										</li>
										<li class="list_item">
											<a href="#">Вы отправляете в мой город/страну?</a>
										</li>
									</ul>
									<ul class="list">
										<div class="list_title">
											<h1>Общая информация</h1>
										</div>
										<li class="list_item">
											<a href="#">О магазине</a>
										</li>
										<li class="list_item">
											<a href="#">Доставка и оплата</a>
										</li>
										<li class="list_item">
											<a href="#">Доставка и оплата</a>
										</li>
										<li class="list_item">
											<a href="#">Возврат и обмен</a>
										</li>
										<li class="list_item">
											<a href="#">Руководство по размеру</a>
										</li>
									</ul>
									<ul class="list">
										<div class="list_title">
											<h1>Контакты</h1>
										</div>
										<li class="list_item">
											<a href="#">+38 (096) 123 45 67</a>
										</li>
										<li class="list_item">
											<a href="#">+38 (096) 123 45 67</a>
										</li>
										<li class="list_item">
											<a href="#">komarov@gmail.com</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="list_item mobile">
					<span class="category_name">Аккаунт</span>
					<div class="arrow_container">
						<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">
					</div>
					<div class="category_menu">
						<div class="category_container">
							<div class="category_header">
								<div class="menu_back">
									<img src="{{ asset('images/main/images/category-back-arrow.png') }}" alt="">
								</div>
								<div class="menu_logo">
									<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
								</div>
								<div class="menu_search hide">
									<img src="{{ asset('images/main/images/search-icon.png') }}" alt="">
								</div>
							</div>
							<div class="category_body">
								<div class="account_container">
									<form action="#" class="form_container">
										<div class="form-group">
											<label>
												<p class="text">Email адрес*</p>
												<a class="create_link" href="#">Создать аккаунт</a>
											</label>
											<input type="mail" placeholder="@gmail.com">
										</div>
										<div class="form-group last">
											<label>
												<p class="text">Пароль*</p>
											</label>
											<input type="password">
										</div>
										<div class="bottom_submit">
											<div class="pass_forgot">
												<a href="#">Я забыл свой пароль</a>
											</div>
											<div class="btn_container">
												<button type="submit" class="custom_btn">Войти</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="menu_overlay"></div>
</div>
{{--tablet--}}
<div class="mobile_menu d-none d-sm-block d-xs-none">
	<div class="menu_container">
		<div class="menu_header">
			<div class="menu_close">
				<img src="{{ asset('images/main/images/close-icon.png') }}" alt="">
			</div>
			<div class="menu_logo">
				<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
			</div>
			<div class="menu_search">
				<img src="{{ asset('images/main/images/search-icon.png') }}" alt="">
			</div>
		</div>
		<div class="menu_body">
			<div class="row m-0">
				@include('layouts.top_menu_tablet', ['categories' => $categories])
				{{--<div class="col-md-3 col-sm-4 p-0 left_bar">--}}
					{{--<ul class="list_nav">--}}

						{{--<li id="brills" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Бриллианты</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="earrings" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Серьги</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="rings" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Кольца</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="chains" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Цепочки</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="pendants" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Подвески</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="biju" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Бижутерия</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="crests" class="list_item mobile_higher">--}}
							{{--<a href="#" class="category_name">Крестики</a>--}}
						{{--</li>--}}
						{{--<li id="pins" class="list_item mobile_higher">--}}
							{{--<a href="#" class="category_name">Булавки</a>--}}
						{{--</li>--}}
					{{--</ul>--}}
					{{--<ul class="list_nav list_footer">--}}
						{{--<li id="help" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Помощь</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
						{{--<li id="acc" class="list_item mobile_higher">--}}
							{{--<span class="category_name">Аккаунт</span>--}}
							{{--<div class="arrow_container">--}}
								{{--<img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</li>--}}
					{{--</ul>--}}
				{{--</div>--}}
				{{--<div class="col-md-9 col-sm-8 p-0 right_bar">--}}
					{{--<div data-categ="brills" class="category_menu">--}}
						{{--<div class="category_container">--}}
							{{--<div class="category_body">--}}
								{{--<div class="row m-0">--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С бриллиантами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
						{{--<div class="btn_container">--}}
							{{--<a href="#" class="custom_btn">Перейти в каталог</a>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div data-categ="earrings" class="category_menu">--}}
						{{--<div class="category_container">--}}
							{{--<div class="category_body">--}}
								{{--<div class="row m-0">--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
										{{--<a href="#">--}}
											{{--<div class="category_card">--}}
												{{--<div class="category_image">--}}
													{{--<img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">--}}
												{{--</div>--}}
												{{--<div class="category_name">--}}
													{{--<p>С серьгами</p>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</a>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
						{{--<div class="btn_container">--}}
							{{--<a href="#" class="custom_btn">Перейти в каталог</a>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div data-categ="help" class="category_menu">--}}
						{{--<div class="category_container">--}}
							{{--<div class="category_body">--}}
								{{--<div class="freq_questions">--}}
									{{--<div class="row m-0">--}}
										{{--<div class="col-lg-5 col-md-12 col-sm-12 p-0">--}}
											{{--<ul class="list">--}}
												{{--<div class="list_title">--}}
													{{--<h1>Часто задаваемые вопросы</h1>--}}
												{{--</div>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Мне нужна учётная запись для покупки?</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Сколько стоит доставка?</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Как мне вернуть или обменять?</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Вы отправляете в мой город/страну?</a>--}}
												{{--</li>--}}
											{{--</ul>--}}
										{{--</div>--}}
										{{--<div class="col-lg-4 col-md-6 col-sm-12 p-0">--}}
											{{--<ul class="list">--}}
												{{--<div class="list_title">--}}
													{{--<h1>Общая информация</h1>--}}
												{{--</div>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">О магазине</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Доставка и оплата</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Доставка и оплата</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Возврат и обмен</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">Руководство по размеру</a>--}}
												{{--</li>--}}
											{{--</ul>--}}
										{{--</div>--}}
										{{--<div class="col-lg-3 col-md-6 col-sm-12 p-0">--}}
											{{--<ul class="list">--}}
												{{--<div class="list_title">--}}
													{{--<h1>Контакты</h1>--}}
												{{--</div>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">+38 (096) 123 45 67</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">+38 (096) 123 45 67</a>--}}
												{{--</li>--}}
												{{--<li class="list_item">--}}
													{{--<a href="#">komarov@gmail.com</a>--}}
												{{--</li>--}}
											{{--</ul>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div data-categ="acc" class="category_menu account">--}}
						{{--<div class="category_container">--}}
							{{--<div class="category_body">--}}
								{{--<div class="account_container">--}}
									{{--<form action="#" class="form_container">--}}
										{{--<div class="form-group">--}}
											{{--<label>--}}
												{{--<p class="text">Email адрес*</p>--}}
												{{--<a class="create_link" href="#">Создать аккаунт</a>--}}
											{{--</label>--}}
											{{--<input type="mail" placeholder="@gmail.com">--}}
										{{--</div>--}}
										{{--<div class="form-group last">--}}
											{{--<label>--}}
												{{--<p class="text">Пароль*</p>--}}
											{{--</label>--}}
											{{--<input type="password">--}}
										{{--</div>--}}
										{{--<div class="bottom_submit">--}}
											{{--<div class="pass_forgot">--}}
												{{--<a href="#">Я забыл свой пароль</a>--}}
											{{--</div>--}}
											{{--<div class="btn_container">--}}
												{{--<button type="submit" class="custom_btn">Войти</button>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</form>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
			</div>
		</div>
	</div>
	<div class="menu_overlay"></div>
</div>

<div class="menu_search_container d-block d-xl-none">
	<div class="search_header">
		<div class="menu_search_back">
			<img src="{{ asset('images/main/images/category-back-arrow.png') }}" alt="">
		</div>
		<div class="menu_logo">
			<img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
		</div>
		<div class="menu_search">
			<img src="{{ asset('images/main/images/search-icon.png') }}" alt="">
		</div>
	</div>
	<div class="search_body">
		<div class="row m-0">
			<div class="col-lg-6 col-12 p-0">
				<form class="group_form" action="#">
					<input class="search_input" name="search" type="text" placeholder="Поиск...">
					<button class="search_btn" type="submit"><img src="{{ asset('images/main/images/arrow-search.png') }}" alt=""></button>
				</form>
			</div>
			<div class="col-lg-6 col-12 p-0">
				<div class="samples_container">
					<div class="title">
						<h1>Примеры поиска</h1>
					</div>
					<div class="text">
						<p>Золотые кольца</p>
						<p>Серебренные Крестики</p>
						<p>Обручальные кольца</p>
						<p>Вы отправляете в мой город/страну?</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>