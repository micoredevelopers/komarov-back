<div class="col-md-3 col-sm-4 p-0 left_bar">
    <ul class="list_nav">
        @foreach ($categories as $category)
        <li id="{{ $category->slug }}" class="list_item mobile_higher">
            <span class="category_name">{{ $category->title }}</span>
            <div class="arrow_container">
                    <img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">
            </div>
        </li>
        @endforeach
    </ul>
    <ul class="list_nav list_footer">
        <li id="help" class="list_item mobile_higher">
            <span class="category_name">Помощь</span>
            <div class="arrow_container">
                <img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">
            </div>
        </li>
        <li id="acc" class="list_item mobile_higher">
            <span class="category_name">Аккаунт</span>
            <div class="arrow_container">
                <img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">
            </div>
        </li>
    </ul>
</div>


<div class="col-md-9 col-sm-8 p-0 right_bar">
    @foreach ($categories as $category)
        <div data-categ="{{ $category->slug }}" class="category_menu">

            @if ($category->children->count())
            <div class="category_container">
                <div class="category_body">
                    <div class="row m-0">
                        @foreach ($category->children as $cat_child )
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <a href="{{ url("/category/$cat_child->slug")}}">
                                <div class="category_card">
                                    <div class="category_image">
                                        @if (isset($cat_child->image))
                                            <img src="{{ asset('images/category' . '/' . $cat_child->image) }} " alt="">
                                        @else
                                            <img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">
                                        @endif
                                    </div>
                                    <div class="category_name">
                                        <p>{{ $cat_child->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
            @endif
            <div class="btn_container">
                <a href="{{ url("/category/$category->slug")}}" class="custom_btn">Перейти в каталог</a>
            </div>
        </div>


    @endforeach

    <div data-categ="help" class="category_menu">
        <div class="category_container">
            <div class="category_body">
                <div class="freq_questions">
                    <div class="row m-0">
                        <div class="col-lg-5 col-md-12 col-sm-12 p-0">
                            <ul class="list">
                                <div class="list_title">
                                    <h1>Часто задаваемые вопросы</h1>
                                </div>
                                <li class="list_item">
                                    <a href="#">Мне нужна учётная запись для покупки?</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Сколько стоит доставка?</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Как мне вернуть или обменять?</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Вы отправляете в мой город/страну?</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 p-0">
                            <ul class="list">
                                <div class="list_title">
                                    <h1>Общая информация</h1>
                                </div>
                                <li class="list_item">
                                    <a href="#">О магазине</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Доставка и оплата</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Доставка и оплата</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Возврат и обмен</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">Руководство по размеру</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 p-0">
                            <ul class="list">
                                <div class="list_title">
                                    <h1>Контакты</h1>
                                </div>
                                <li class="list_item">
                                    <a href="#">+38 (096) 123 45 67</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">+38 (096) 123 45 67</a>
                                </li>
                                <li class="list_item">
                                    <a href="#">komarov@gmail.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-categ="acc" class="category_menu account">
        <div class="category_container">
            <div class="category_body">
                <div class="account_container">
                    <form action="#" class="form_container">
                        <div class="form-group">
                            <label>
                                <p class="text">Email адрес*</p>
                                <a class="create_link" href="#">Создать аккаунт</a>
                            </label>
                            <input type="mail" placeholder="@gmail.com">
                        </div>
                        <div class="form-group last">
                            <label>
                                <p class="text">Пароль*</p>
                            </label>
                            <input type="password">
                        </div>
                        <div class="bottom_submit">
                            <div class="pass_forgot">
                                <a href="#">Я забыл свой пароль</a>
                            </div>
                            <div class="btn_container">
                                <button type="submit" class="custom_btn">Войти</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>