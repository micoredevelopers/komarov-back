@foreach ($categories as $category)
<li class="list_item mobile">
    <span class="category_name">{{ $category->title }}</span>
    <div class="arrow_container">
        <img src="{{ asset('images/main/images/menu-arrow-right.png') }}" alt="">
    </div>
    <div class="category_menu">
        <div class="category_container">
            <div class="category_header">
                <div class="menu_back">
                    <img src="{{ asset('images/main/images/category-back-arrow.png') }}" alt="">
                </div>
                <div class="menu_logo">
                    <img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt="">
                </div>
                <div class="menu_search hide">
                    <img src="{{ asset('images/main/images/search-icon.png') }}" alt="">
                </div>
            </div>

            {{--Если у категорий есть вложенные пункты, тогда ...--}}
            @if ($category->children->count())
            <div class="category_body">
                <div class="row m-0">

                    @foreach ($category->children as $cat_child )
                    <div class="col-6">
                        <a href="{{ url("/category/$cat_child->slug")}}">
                            <div class="category_card">
                                <div class="category_image">
                                    @if (isset($cat_child->image))
                                        <img src="{{ asset('images/category' . '/' . $cat_child->image) }} " alt="">
                                    @else
                                        <img src="{{ asset('images/main/images/category-menu-image.png') }}" alt="">
                                    @endif
                                </div>
                                <div class="category_name">
                                    <p>{{ $cat_child->title }}</p>
                                </div>
                            </div>
                        </a>
                    </div>

                    @endforeach

                </div>

                <div class="btn_container">
                    <a class="custom_btn" href="{{ url("/category/$category->slug")}}">Перейти в каталог</a>
                </div>

            </div>


            @endif
        </div>
    </div>
</li>
@endforeach