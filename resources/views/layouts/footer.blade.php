<footer>
    <div class="container">
        <div class="row m-0">
            <div class="col-xl-3 col-sm-6 col-12 p-0 left_block">
                <div class="logo">
                    <img src="{{ asset('images/main/images/logo-mobile-white.svg') }}" alt="">
                </div>
                <div class="text_about">
                    <p>Любовь не измерить каратами, но с их помощью довольно легко продемонстрировать глубину своих чувств по отношению к
                        любимой женщине.</p>
                </div>
                <div class="socials_container">
                    <a href="#">
                        <img src="{{ asset('images/main/images/fb_icon.png') }}" alt="">
                    </a>
                    <a href="#">
                        <img src="{{ asset('images/main/images/inst_icon.png') }}" alt="">
                    </a>
                    <a href="#">
                        <img src="{{ asset('images/main/images/twitter_icon.png') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12 p-0 right_block">
                <h1 class="list_title">Изделия</h1>
                <div class="list_container">
                    <ul class="nav_list first_child">
                        <li class="list_item">
                            <a href="#">Кольца</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Серьги</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Цепочки</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Подвески</a>
                        </li>
                    </ul>
                    <ul class="nav_list second_child">
                        <li class="list_item">
                            <a href="#">Бриллианты</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Крестики</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Булавки</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Бижутерия</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12 p-0 right_block">
                <h1 class="list_title">Общая информация</h1>
                <div class="list_container">
                    <ul class="nav_list">
                        <li class="list_item">
                            <a href="#">О магазине</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Доставка и оплата</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Гарантии</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Возврат и обмен</a>
                        </li>
                        <li class="list_item">
                            <a href="#">Руководство по размеру</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12 p-0 right_block">
                <h1 class="list_title">Контакты</h1>
                <div class="list_container">
                    <ul class="nav_list">
                        <li class="list_item">
                            <p>+38 (096) 123 45 67</p>
                        </li>
                        <li class="list_item">
                            <p>+38 (096) 123 45 67</p>
                        </li>
                        <li class="list_item">
                            <p>komarov@gmail.com</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyright_text">
            <p>komarov.com (c) 2018, with love
                <a href="#">by Command+X</a>
            </p>
        </div>
    </div>
</footer>

<div class="body_overlay"></div>