@foreach ($categories->slice(0, 3) as $category)
            <li class="list_item">
                <p>{{ $category->title }}</p>
                <div class="desktop_menu d-none d-xl-block">
                    <div class="container">
                        <div class="desktop_menu_header">
                            <div class="logo"><img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt=""></div>
                            <div class="title"><h1>{{ $category->title }}</h1></div>
                            <div class="close_icon"><img src="{{ asset('images/main/images/close-icon.png') }}" alt=""></div>
                        </div>
                        {{--Если у категорий есть вложенные пункты, тогда ...--}}
                        @if ($category->children->count())


                        <div class="desktop_menu_body">
                            <div class="row m-0">

                                @foreach ($category->children as $cat_child )

                                <div class="col-xl-3">

                                    <a href="{{ url("/category/$cat_child->slug")}}">
                                        <div class="category_block">
                                            <div class="product_image">
                                                @if (isset($cat_child->image))
                                                    <img src="{{ asset('images/category' . '/' . $cat_child->image) }} " alt="">
                                                @else
                                                    <img src="{{ asset('images/main/images/sub-category-image.png') }}" alt="">
                                                @endif

                                            </div>
                                            <div class="product_name"><p>{{ $cat_child->title }}</p></div>
                                        </div>
                                    </a>
                                </div>
                                    @endforeach
                            </div>
                            <div class="btn_container">
                                <a class="custom_btn" href="{{ url("/category/$category->slug")}}">Перейти в каталог</a>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </li>
        @endforeach


        <li class="list_item">
            <p>Другое</p>
            <div class="desktop_menu d-none d-xl-block">
                <div class="container">
                    <div class="desktop_menu_header">
                        <div class="logo"><img src="{{ asset('images/main/images/logo-mobile.svg') }}" alt=""></div>
                        <div class="title"><h1>Другое</h1></div>
                        <div class="close_icon"><img src="{{ asset('images/main/images/close-icon.png') }}" alt=""></div>
                    </div>
                    <div class="desktop_menu_body">
                        <div class="row m-0">
                            @foreach ($categories->slice(3) as $category)
                            <div class="col-xl-3">
                                <a href="{{ url("/category/$category->slug")}}">
                                    <div class="category_block">
                                        <div class="product_image"><img src="{{ asset('images/main/images/sub-category-image.png') }}" alt=""></div>
                                        <div class="product_name"><p>{{ $category->title }}</p></div>
                                    </div>
                                </a>
                            </div>

                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        </li>