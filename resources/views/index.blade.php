@extends('layouts.app')


@section('title', 'Komarov | Main')
@section('meta_keyword', '')
@section('meta_description', '')

@section('wrap-class')
    <div id="wrapper-main">
@endsection

@section('content')
        <main>
            <section class="section_slide">
                <div class="bg_overlay"></div>
                <div class="main-carousel">
                    <div class="carousel-cell">
                        <div class="image_container">
                            <img src="{{ asset('images/main/images/main/main-slide-image.png') }}" alt="">
                        </div>
                        <div class="slide_text">
                            <h3 class="sub_title">gold and silver</h3>
                            <h1 class="title">Shop the new collection with free shipping</h1>
                        </div>
                    </div>
                    <div class="carousel-cell">
                        <div class="image_container">
                            <img src="{{ asset('images/main/images/main/main-slide-image.png') }}" alt="">
                        </div>
                        <div class="slide_text">
                            <h3 class="sub_title">gold and silver</h3>
                            <h1 class="title">Shop the new collection with free shipping</h1>
                        </div>
                    </div>
                    <div class="carousel-cell">
                        <div class="image_container">
                            <img src="{{ asset('images/main/images/main/main-slide-image.png') }}" alt="">
                        </div>
                        <div class="slide_text">
                            <h3 class="sub_title">gold and silver</h3>
                            <h1 class="title">Shop the new collection with free shipping</h1>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section_second_slide">
                <div class="slide_container news">
                    <h1 class="slide_title">Новинки</h1>
                    <div class="products-news-carousel">

                        @foreach ($new_products as $new_product)
                            @php $product['images'] = json_decode($new_product['images']) @endphp
                            <div class="carousel-cell">
                                <div class="image_container">

                                    @isset($product['images'])
                                        @foreach($product['images'] as $image)
                                            @isset($image)
                                                {{--если это видео и есть превью показываем его--}}
                                                @if (ends_with($image, '.mp4'))
                                                    <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $image) ) }}" alt="">
                                                @elseif (ends_with($image, '.mov'))
                                                    <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $image) ) }}" alt="">
                                                @else
                                                    <img src=" {{ Storage::disk('upload')->url($image ) }}" alt="">
                                                @endif
                                                @break
                                            @endisset
                                        @endforeach
                                    @endisset
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>



                <div class="slide_container popular">
                    <h1 class="slide_title">Популярные товары</h1>
                    <div class="products-news-carousel">

                        @foreach ($popular_products as $popular_product)
                            @php $product['images'] = json_decode($popular_product[0]['product']['images']) @endphp
                        <div class="carousel-cell">
                            <div class="image_container">

                                @isset($product['images'])
                                    @foreach($product['images'] as $image)
                                        @isset($image)
                                            {{--если это видео и есть превью показываем его--}}
                                            @if (ends_with($image, '.mp4'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $image) ) }}" alt="">
                                            @elseif (ends_with($image, '.mov'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $image) ) }}" alt="">
                                            @else
                                                <img src=" {{ Storage::disk('upload')->url($image ) }}" alt="">
                                            @endif
                                            @break
                                        @endisset
                                    @endforeach
                                @endisset
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>



            </section>

            <section class="section_categories">
                <div class="container-fluid p-0">
                    <div class="row m-0">
                        <div class="col-12 p-0">
                            <div class="categ_product_block rings">
                                <div class="bottom_img left">
                                    <img src="{{ asset('images/main/images/main/ring-img.png') }}" alt="">
                                </div>
                                <div class="bottom_img right">
                                    <img src="{{ asset('images/main/images/main/ring-img-2.png') }}" alt="">
                                </div>
                                <div class="bg_overlay"></div>
                                <div class="inner_block">
                                    <div class="product_info_container">
                                        <h1 class="title">{{ $categories[2]->title }}</h1>
                                        <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize bounce
                                            while maximizing.
                                        </p>
                                        <div class="btn_container">
                                            <a href="{{ url("/category/{$categories[2]->slug}")}}">
                                                <span>Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-0">
                        <div class="col-md-6 col-12 p-0 full_height">
                            <div class="categ_product_block chains">
                                <div class="bottom_img right">
                                    <img src="{{ asset('images/main/images/main/pendant-img-2.png') }}" alt="">
                                </div>
                                <div class="bg_overlay"></div>
                                <div class="inner_block">
                                    <div class="product_info_container to-left">
                                        <h1 class="title">{{ $categories[3]->title }}</h1>
                                        <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize bounce
                                            while maximizing.
                                        </p>
                                        <div class="btn_container to-left">
                                            <a href="{{ url("/category/{$categories[3]->slug}")}}">
                                                <span>Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-12 p-0 full_height_right">
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <div class="categ_product_block earrings">
                                        <div class="bottom_img left">
                                            <img src="{{ asset('images/main/images/main/earring-img.png') }}" alt="">
                                        </div>
                                        <div class="bg_overlay"></div>
                                        <div class="inner_block">
                                            <div class="product_info_container to-right">
                                                <h1 class="title">{{ $categories[1]->title }}</h1>
                                                <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize
                                                    bounce while maximizing.
                                                </p>
                                                <div class="btn_container to-right">
                                                    <a href="{{ url("/category/{$categories[1]->slug}")}}">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="categ_product_block pendants">
                                        <div class="bottom_img right">
                                            <img src="{{ asset('images/main/images/main/pendant-img-2.png') }}" alt="">
                                        </div>
                                        <div class="bg_overlay"></div>
                                        <div class="inner_block">
                                            <div class="product_info_container to-left">
                                                <h1 class="title">{{ $categories[4]->title }}</h1>
                                                <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize
                                                    bounce while maximizing.
                                                </p>
                                                <div class="btn_container to-left">
                                                    <a href="{{ url("/category/{$categories[4]->slug}")}}">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-12 p-0">
                            <div class="categ_product_block brilliants">
                                <div class="bottom_img center">
                                    <img src="{{ asset('images/main/images/main/brilliants-img.png') }}" alt="">
                                </div>
                                <div class="bg_overlay"></div>
                                <div class="inner_block">
                                    <div class="product_info_container">
                                        <h1 class="title">{{ $categories[0]->title }}</h1>
                                        <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize bounce
                                            while maximizing.
                                        </p>
                                        <div class="btn_container">
                                            <a href="{{ url("/category/{$categories[0]->slug}")}}">
                                                <span>Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-md-6 col-12 p-0 full_height_left">
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <div class="categ_product_block crosses">
                                        <div class="bottom_img left">
                                            <img src="{{ asset('images/main/images/main/cross-img.png') }}" alt="">
                                        </div>
                                        <div class="bg_overlay"></div>
                                        <div class="inner_block">
                                            <div class="product_info_container to-right">
                                                <h1 class="title">{{ $categories[5]->title }}</h1>
                                                <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize
                                                    bounce while maximizing.
                                                </p>
                                                <div class="btn_container to-right">
                                                    <a href="{{ url("/category/{$categories[5]->slug}")}}">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="categ_product_block biju">
                                        <div class="bottom_img right">
                                            <img src="{{ asset('images/main/images/main/pendant-img-2.png') }}" alt="">
                                        </div>
                                        <div class="bg_overlay"></div>
                                        <div class="inner_block">
                                            <div class="product_info_container to-left">
                                                <h1 class="title">{{ $categories[6]->title }}</h1>
                                                <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize
                                                    bounce while maximizing.
                                                </p>
                                                <div class="btn_container to-left">
                                                    <a href="{{ url("/category/{$categories[6]->slug}")}}">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 p-0 full_height right_side">
                            <div class="categ_product_block pins">
                                <div class="bottom_img center">
                                    <img src="{{ asset('images/main/images/main/pendant-img-2.png') }}" alt="">
                                </div>
                                <div class="bg_overlay"></div>
                                <div class="inner_block">
                                    <div class="product_info_container">
                                        <h1 class="title">{{ $categories[7]->title }}</h1>
                                        <p class="text">The legendary 1988 silhouette, now with an all-white upper and ice-blue outsole. Sports bras made to minimize bounce
                                            while maximizing.
                                        </p>
                                        <div class="btn_container">
                                            <a href="{{ url("/category/{$categories[7]->slug}")}}">
                                                <span>Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section_about_info">
                <div class="container">
                    <div class="text_info">
                        <h1 class="title">Дом Комаровых. Изделия высшего качества.</h1>
                        <p class="text">
                            Любовь не измерить каратами, но с их помощью довольно легко продемонстрировать глубину своих чувств по отношению к любимой
                            женщине или мужчине. Драгоценные камни и металлы считаются таковыми не только из-за своей красоты, но и по причине
                            малого количества относительно других металлов и минералов, встречающихся в природе. Когда мужчина дарит золотое кольцо
                            или серьги любимой женщине, он тем самым демонстрирует, насколько дорогой ему этот человек.
                        </p>
                        <p class="text">
                            Наша ювелирная сеть создана в 1999 году, на границе нового тысячелетия, и нашей целью было дать возможность людям привнести
                            в свою жизнь немного яркости и красоты. В нашем магазине ювелирных изделий представлены украшения, которые помогут
                            выразить глубокие чувства по отношению к близким людям. Наша цель с самого начала существования – возродить истинное
                            ювелирное искусство, даруя миру изысканные украшения с оригинальным дизайном, удовлетворяя потребности даже наиболее
                            взыскательных и требовательных клиентов. Над каждым украшением трудятся профессионалы своего дела, работающие в нашей
                            собственной дизайн-студии.
                        </p>
                        <p class="text">
                            Главная цель каждого сотрудника – творить ювелирные украшения, в которых гармонично сплетены современная мода и классические
                            мотивы. Для реализации и производства украшений используется передовое оборудование "SOLIDSCAPE" и "ROLAND", на котором
                            работают настоящие профессионалы своего дела, обладающие твердой рукой и четким пониманием специфики ювелирного дела.
                        </p>
                        <p class="text">
                            Главная цель каждого сотрудника – творить ювелирные украшения, в которых гармонично сплетены современная мода и классические
                            мотивы. Для реализации и производства украшений используется передовое оборудование "SOLIDSCAPE" и "ROLAND", на котором
                            работают настоящие профессионалы своего дела, обладающие твердой рукой и четким пониманием специфики ювелирного дела.
                        </p>
                        <p class="text">
                            Главная цель каждого сотрудника – творить ювелирные украшения, в которых гармонично сплетены современная мода и классические
                            мотивы. Для реализации и производства украшений используется передовое оборудование "SOLIDSCAPE" и "ROLAND", на котором
                            работают настоящие профессионалы своего дела, обладающие твердой рукой и четким пониманием специфики ювелирного дела.
                        </p>
                        <p class="text">
                            Главная цель каждого сотрудника – творить ювелирные украшения, в которых гармонично сплетены современная мода и классические
                            мотивы. Для реализации и производства украшений используется передовое оборудование "SOLIDSCAPE" и "ROLAND", на котором
                            работают настоящие профессионалы своего дела, обладающие твердой рукой и четким пониманием специфики ювелирного дела.
                        </p>
                    </div>
                    <div class="btn_container">
                        <a class="my_btn">Подробнее</a>
                    </div>
                </div>
            </section>

        </main>
@endsection





