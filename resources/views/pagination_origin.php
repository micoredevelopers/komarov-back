{{--Pagination--}}
<div class="pagination_nav">
    <div class="pagination_container">
        @if($products->lastPage() > 1)
        {{--для отображения стрелочки "Предыдущая страница"--}}
        @if($products->currentPage() !== 1)
        <div class="arrow_container arrow_prev">
            <a href="{{ $products->url(($products->currentPage() - 1 )) }}">
                <img src="{{ asset('images/main/images/products-list/pagination-arrow-left.png') }}" alt="">
            </a>
        </div>
        @endif
        <ul class="pag_list">
            @for($i = 1; $i <= $products->lastPage(); $i++)
            @if( $i == $products->currentPage())
            <li class="pag_item active"><a class="page_numb" >{{ $i }}</a></li>
            @else
            @if ($i == 1)
            <li class="pag_item first"><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
            @endif

            @if ($i == ($products->currentPage() - 1) && ($i !== 1 ))
            @if(($products->currentPage() - 2) !== 1)
            <li class="pag_item dots"><span class="page_dots">...</span></li>
            @endif
            <li class="pag_item"><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
            @endif

            @if ( $i == ($products->currentPage() + 1) && ( $i !== $products->lastPage() ) )
            <li class="pag_item mr-0" ><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
            @if(($products->currentPage() + 2) !== $products->lastPage())
            <li class="pag_item dots"><span class="page_dots">...</span></li>
            @endif
            @endif
            @if ($i == $products->lastPage())
            <li class="pag_item last"><a class="page_numb" href="{{ $products->url($i) }}">{{ $i }}</a></li>
            @endif

            @endif
            @endfor
        </ul>
        {{--для отображения стрелочки "Последующая страница страница"--}}
        @if($products->currentPage() !== $products->lastPage())
        <div class="arrow_container arrow_next">
            <a href="{{ $products->url(($products->currentPage() + 1 )) }}">
                <img src="{{ asset('images/main/images/products-list/pagintaion-arrow-right.png') }}" alt="">
            </a>
        </div>
        @endif
        @endif
    </div>
</div>