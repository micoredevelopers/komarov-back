<h1>{{$title}}</h1>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('index')}}">{{$parent}}</a></li>
    <li class=" breadcrumb-item active">{{ $active }}</li>
</ol>