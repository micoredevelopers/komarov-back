@extends('admin.layouts.app_admin')


@section('wrap-class')
	<div id="wrapper-users">
@endsection

@section('navigation')
	@include('admin.navigation')
@endsection
 
@section('content')

	<main>
		<div class="section_main">
			<div class="container-fluid p-0">
				<div class="row m-0">
					<div class="col-12 p-0">
						<div class="header_search">
							<div class="title">
								<h1>Пользователи</h1>
							</div>
							<form action="{{ route('adminUserSearch') }}" method="GET">
								<div class="input_container">
									<button type="submit"><img src="{{ asset('images/admin/search_icon.png') }}" alt=""></button>
									<input id="search" type="text" name="search" placeholder="Поиск">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="table_main row m-0">
					<div class="col-12 p-0">
						<div class="table_container">
							<table>
								<thead>
								<tr>
									<th>
										<p>Имя</p>
									</th>
									<th>
										<p>Фамилия</p>
									</th>
									<th>
										<p>Телефон</p>
									</th>
									<th>
										<p>Mail</p>
									</th>
									<th>
										<p>Пол</p>
									</th>
									<th>
										<p>Статус</p>
									</th>
									<th>
										<p>Действие</p>
									</th>
								</tr>
								</thead>
								<tbody>
								{{--@forelse($users['data'] as $user)--}}
								{{--<tr>--}}
									{{--<th>--}}
										{{--<p>{{ isset($user['firstname']) ? $user['firstname'] : ''}}</p>--}}
									{{--</th>--}}
									{{--<th>--}}
										{{--<p>{{ isset($user['lastname']) ? $user['lastname'] : '' }}</p>--}}
									{{--</th>--}}
									{{--<th>--}}
										{{--<p>{{ isset($user['phone']) ? $user['phone'] : '' }}</p>--}}
									{{--</th>--}}
									{{--<th>--}}
										{{--<p>{{ isset($user['email']) ? $user['email'] : '' }}</p>--}}
									{{--</th>--}}
									{{--<th>--}}
										{{--<p>{{ isset($user['sex']) ? $user['sex'] : '' }}</p>--}}
									{{--</th>--}}
									{{--<th>--}}
										{{--<p>@if ($user['status']) Активен @else Заблокирован@endif</p>--}}
									{{--</th>--}}
									{{--<th>--}}
										{{--<div class="order_button">--}}
											{{--<img class="btn_show" src="{{ asset('images/admin/button_detailed_icon.png') }}" alt="">--}}
											{{--<div class=" detailed_block">--}}
											{{--<a class="text_btn" href="{{route('adminUserBlock', $user)}}">@if ($user['status']) Заблокировать @else Разблокировать @endif</a>--}}
											{{--<form class="text_btn delete" action="{{route('admin.user.destroy', $user)}}" method="post">--}}
												{{--<input type="hidden" name="_method" value="delete">--}}
												{{--{{ csrf_field() }}--}}
												{{--<button type="submit" class="text_btn" href="{{route('admin.user.destroy', $user)}}">Удалить</button>--}}
											{{--</form>--}}
											{{--</div>--}}
										{{--</div>--}}

									{{--</th>--}}
								{{--</tr>--}}
								{{--@empty--}}
									{{--<tr>--}}
										{{--<td>--}}
											{{--<h3 style="text-alight:center">Данные отсутствуют</h3>--}}
										{{--</td>--}}
									{{--</tr>--}}
								{{--@endforelse--}}
								{{--</tbody>--}}
							{{--</table>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--@php dd($users) @endphp--}}
				{{--@if($users['last_page'] > 1)--}}
					{{--<div class="pagination_container">--}}

						{{--<div class="page_count">--}}
							{{--<p class="page_numb">{{ $users['current_page'] }}</p>--}}
							{{--<span>из</span>--}}
							{{--<p class="page_numb">{{ $users['last_page']}}</p>--}}
						{{--</div>--}}
						{{--<div class="nav_container">--}}
							{{--для отображения стрелочки "Предыдущая страница"--}}
							{{--@if($users['current_page'] !== 1)--}}
								{{--<a href="{{ $users['last_page_url'] }}"><img src="{{ asset('images/admin/arrow_left.png') }}" alt=""></a>--}}
							{{--@endif--}}
							{{--@if($users['current_page'] !== $users['last_page'])--}}
								{{--<a href="{{ $users['next_page_url'] }}"><img src="{{ asset('images/admin/arrow_right.png') }}" alt=""></a>--}}
							{{--@endif--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--@endif--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--</main>--}}


								@forelse($users as $user)
									<tr>
										<th>
											<p>{{ isset($user->user_additional->firstname) ? $user->user_additional->firstname : ''}}</p>
										</th>
										<th>
											<p>{{ isset($user->user_additional->lastname) ? $user->user_additional->lastname : '' }}</p>
										</th>
										<th>
											<p>{{ isset($user->user_additional->phone) ? $user->user_additional->phone : '' }}</p>
										</th>
										<th>
											<p>{{ isset($user->email) ? $user->email : '' }}</p>
										</th>
										<th>
											<p>{{ isset($user->user_additional->sex) ? $user->user_additional->sex : '' }}</p>
										</th>
										<th>
											<p>@if ($user->status) Активен @else Заблокирован@endif</p>
										</th>
										<th>
											<div class="order_button">
												<img class="btn_show" src="{{ asset('images/admin/button_detailed_icon.png') }}" alt="">
												<div class=" detailed_block">
													<a class="text_btn" href="{{route('adminUserBlock', $user)}}">@if ($user->status) Заблокировать @else Разблокировать @endif</a>
													<form class="text_btn delete" action="{{route('admin.user.destroy', $user)}}" method="post">
														<input type="hidden" name="_method" value="delete">
														{{ csrf_field() }}
														<button type="submit" class="text_btn" href="{{route('admin.user.destroy', $user)}}">Удалить</button>
													</form>
												</div>
											</div>

										</th>
									</tr>
								@empty
									<tr>
										<td>
											<h3 style="text-alight:center">Данные отсутствуют</h3>
										</td>
									</tr>
								@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
				@if($users->lastPage() > 1)
					<div class="pagination_container">

						<div class="page_count">
							<p class="page_numb">{{ $users->currentPage() }}</p>
							<span>из</span>
							<p class="page_numb">{{ $users->lastPage() }}</p>
						</div>
						<div class="nav_container">
							{{--для отображения стрелочки "Предыдущая страница"--}}
							@if($users->currentPage() !== 1)
								<a href="{{ $users->url(($users->currentPage() - 1 )) }}"><img src="{{ asset('images/admin/arrow_left.png') }}" alt=""></a>
							@endif
							@if($users->currentPage() !== $users->lastPage())
								<a href="{{ $users->url(($users->currentPage() + 1 )) }}"><img src="{{ asset('images/admin/arrow_right.png') }}" alt=""></a>
							@endif
						</div>
					</div>
				@endif
			</div>
		</div>
	</main>





@endsection