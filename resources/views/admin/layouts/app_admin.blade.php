<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Komarov | Admin</title>

    <!-- Fonts -->
    {{-- <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css"> --}}
    
<!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <!--My Styles -->
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-reboot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/main.css') }}">


    <!-- Font Awesome -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <!-- MyFont -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
</head>
<body>

    @yield('wrap-class')
{{-- <div id="wrapper-products"> --}}
    @yield('navigation')

    @yield('content')

</div>
<!-- Scripts -->
{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

<!-- MyScripts -->
<script src="{{ asset('js/admin/jquery-3.3.1.js') }}" defer></script>
<script src="{{ asset('js/admin/bootstrap.js') }}" defer></script>
<script src="{{ asset('js/admin/main.js') }}" defer></script>

</body>
</html>
