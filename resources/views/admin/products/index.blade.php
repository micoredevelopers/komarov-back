@extends('admin.layouts.app_admin')


@section('wrap-class')
	<div id="wrapper-products">
@endsection

@section('navigation')
	@include('admin.navigation')
@endsection
 
@section('content')

<main>
	<div class="section_main">
		<div class="container-fluid p-0">
			<div class="row m-0">
				<div class="col-12 p-0">
					<div class="header_search">
						<div class="title">
							<h1>Товары</h1>
						</div>
						<form action="{{ route('adminSearch') }}" method="GET">
							<div class="input_container">
								<button type="submit"><img src="{{ asset('images/admin/search_icon.png') }}" alt=""></button>
								<input id="search" type="text" name="search" placeholder="Поиск">
							</div>
						</form>
						<div class="add_btn">
							<a href="{{route('admin.product.create')}}"><img src="{{ asset('images/admin/plus - anticon.svg') }}" alt=""></a>
						</div>
					</div>
				</div>
			</div>
			<div class="table_main row m-0">
				<div class="col-12 p-0">
					<div class="table_container">
						<table>
							<thead>
								<tr>
									<th>
										<p>Товар</p>
									</th>
									<th>
										<p>Артикул</p>
									</th>
									<th>
										<p>Подкатегория</p>
									</th>
									<th>
										<p>Категория</p>
									</th>
									<th>
										<p>Наличие</p>
									</th>
									<th>
										<p>Цена, грн</p>
									</th>
									<th>
										<p>Действие</p>
									</th>
								</tr>
							</thead>
							<tbody>

								@forelse($products as $product)
									@php $product->images = json_decode($product->images) @endphp
								<tr>
									<th>
										<div class="product_container">
											<div class="product_image">
												{{--Перебор images и ставим авой первое которое есть--}}
												@isset($product->images)
												@foreach($product->images as $image)
												@isset($image)
													{{--если это видео и есть превью показываем его--}}
													@if (ends_with($image, '.mp4'))
														<img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $image) ) }}" alt="">
													@elseif (ends_with($image, '.mov'))
														<img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $image) ) }}" alt="">
													@else
														<img src=" {{ Storage::disk('upload')->url($image ) }}" alt="">
													@endif
													@break
												@endisset
													@endforeach
												@endisset
											</div>
											<div class="product_name">
												<p>{{ $product->name }}</p>
											</div>
										</div>
									</th>
									<th>
										<p>{{ $product->code }}</p>
									</th>
									<th>
										<p>
											@if($product->category->parent_id) {{ $product->category->title }} @endif
										</p>
									</th>
									<th>
										<p>
											@if(!$product->category->parent_id) {{ $product->category->title }}
											@else {{ $product->category->where('id', $product->category->parent_id)->first()->title }}
												@endif
										</p>
									</th>
									<th>
										<p>@if ($product->state) В наличии @else Отсутствует @endif</p>
									</th>
									<th>
										<p>@if (isset($product->addoptions[0]->price)) {{ $product->addoptions[0]->price }}@endif</p>
									</th>
									<th>
										<div class="order_button">
											<img class="btn_show" src="{{ asset('images/admin/button_detailed_icon.png') }}" alt="">
											<div class="detailed_block">
												<a class="text_btn" href="{{route('admin.product.edit', $product)}}">Редактировать</a>
												<form class="text_btn delete" action="{{route('admin.product.destroy', $product)}}" method="post">
													<input type="hidden" name="_method" value="delete">
													{{ csrf_field() }}
													<button type="submit" class="text_btn" href="{{route('admin.product.destroy', $product)}}">Удалить</button>
												</form>
												<a class="text_btn" href="{{route('adminAbsent', $product)}}">@if ($product->state) Не в наличии @else В наличии @endif</a>
											</div>
										</div>
									</th>
								</tr>
								@empty
								<tr>
									<td>
										<h3 style="text-alight:center">Данные отсутствуют</h3>
									</td>
								</tr>
								@endforelse

							</tbody>
						</table>
					</div>
				</div>
			</div>
			@if($products->lastPage() > 1)
			<div class="pagination_container">
					
				<div class="page_count">
					<p class="page_numb">{{ $products->currentPage() }}</p>
					<span>из</span>
					<p class="page_numb">{{ $products->lastPage() }}</p>
				</div>
				<div class="nav_container">
					{{--для отображения стрелочки "Предыдущая страница"--}}
					@if($products->currentPage() !== 1)
					<a href="{{ $products->url(($products->currentPage() - 1 )) }}"><img src="{{ asset('images/admin/arrow_left.png') }}" alt=""></a>
					@endif
					@if($products->currentPage() !== $products->lastPage())
					<a href="{{ $products->url(($products->currentPage() + 1 )) }}"><img src="{{ asset('images/admin/arrow_right.png') }}" alt=""></a>
					@endif
				</div>
			</div>
			@endif

		</div>
	</div>
</main>

@endsection