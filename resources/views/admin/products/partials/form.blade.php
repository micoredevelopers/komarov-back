<div class="row m-0">
    <div class="product_block col-7 p-0">
        <div class="form_product_info">
            <div class="row m-0">
                <div class="col-12 p-0">
                    <div class="photo_video_block">
                        <div class="images_block">
                            <div class="image_container">
                                <div class="added_icon">
                                    <img src="{{ asset('images/admin/plus-symbol.svg') }}" alt="">
                                </div>
                                <a href="#"><img src="{{ asset('images/admin/x_icon.png') }}" alt=""></a>
                                <label for="image-input1" class="@if($errors->first('images')) error @endif">
                                        @isset($product->images[0])
                                            {{--если это видео и есть превью показываем его--}}
                                            @if (ends_with($product->images[0], '.mp4'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $product->images[0]) ) }}" alt="">
                                            @elseif (ends_with($product->images[0], '.mov'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $product->images[0]) ) }}" alt="">
                                            @else
                                                <img src=" {{ Storage::disk('upload')->url($product->images[0] ) }}" alt="">
                                            @endif
                                        @endisset
                                </label>
                                <input type="file" accept=".jpg,.jpeg.,.png,.mov,.mp4" name="images[]" id="image-input1"
                                       class="image-upload"/>
                                <input type="hidden" name="old_img[]"
                                       value="{{ isset($product->images[0]) ? $product->images[0] : '' }}">
                                <input class="close_img" type="hidden" name="close_img[]" value="">
                            </div>

                            <div class="image_container">
                                <div class="added_icon">
                                    <img src="{{ asset('images/admin/plus-symbol.svg') }}" alt="">
                                </div>
                                <a href="#"><img src="{{ asset('images/admin/x_icon.png') }}" alt=""></a>
                                <label for="image-input2">
                                        @isset($product->images[1])
                                            {{--если это видео и есть превью показываем его--}}
                                            @if (ends_with($product->images[1], '.mp4'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $product->images[1]) ) }}" alt="">
                                            @elseif (ends_with($product->images[1], '.mov'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $product->images[1]) ) }}" alt="">
                                            @else
                                                <img src=" {{ Storage::disk('upload')->url($product->images[1] ) }}" alt="">
                                            @endif
                                        @endisset
                                </label>
                                <input type="file" accept=".jpg,.jpeg.,.png,.mov,.mp4" name="images[]" id="image-input2"
                                       class="image-upload"/>
                                <input type="hidden" name="old_img[]"
                                       value="{{ (isset($product->images[1])) ? $product->images[1] : '' }} ">
                                <input class="close_img" type="hidden" name="close_img[]" value="">
                            </div>
                            <div class="image_container">
                                <div class="added_icon">
                                    <img src="{{ asset('images/admin/plus-symbol.svg') }}" alt="">
                                </div>
                                <a href="#"><img src="{{ asset('images/admin/x_icon.png') }}" alt=""></a>
                                <label for="image-input3">
                                    @isset($product->images[2])
                                        {{--если это видео и есть превью показываем его--}}
                                        @if (ends_with($product->images[2], '.mp4'))
                                            <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $product->images[2]) ) }}" alt="">
                                        @elseif (ends_with($product->images[2], '.mov'))
                                            <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $product->images[2]) ) }}" alt="">
                                            @else
                                        <img src=" {{ Storage::disk('upload')->url($product->images[2] ) }}" alt="">
                                            @endif
                                    @endisset
                                </label>
                                <input type="file" accept=".jpg,.jpeg.,.png,.mov,.mp4" name="images[]" id="image-input3"
                                       class="image-upload"/>
                                <input type="hidden" name="old_img[]"
                                       value="{{ (isset($product->images[2])) ? $product->images[2] : '' }} ">
                                <input class="close_img" type="hidden" name="close_img[]" value="">
                            </div>
                            <div class="image_container">
                                <div class="added_icon">
                                    <img src="{{ asset('images/admin/plus-symbol.svg') }}" alt="">
                                </div>
                                <a href="#"><img src="{{ asset('images/admin/x_icon.png') }}" alt=""></a>
                                <label for="image-input4">
                                        @isset($product->images[3])
                                            {{--если это видео и есть превью показываем его--}}
                                            @if (ends_with($product->images[3], '.mp4'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mp4', '.png', $product->images[3]) ) }}" alt="">
                                            @elseif (ends_with($product->images[3], '.mov'))
                                                <img src=" {{ Storage::disk('upload')->url(str_replace('.mov', '.png', $product->images[3]) ) }}" alt="">
                                            @else
                                                <img src=" {{ Storage::disk('upload')->url($product->images[3] ) }}" alt="">
                                            @endif
                                        @endisset
                                </label>
                                <input type="file" accept=".jpg,.jpeg.,.png,.mov,.mp4" name="images[]" id="image-input4"
                                       class="image-upload"/>
                                <input type="hidden" name="old_img[]"
                                       value="{{ (isset($product->images[3])) ? $product->images[3] : '' }}">
                                <input class="close_img" type="hidden" name="close_img[]" value="">
                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Название товара</p>
                                    <input name="name" type="text" placeholder="Золотое кольцо"
                                           class="@if($errors->first('name')) error @endif"
                                           value="{{ (isset($product->name)) ? $product->name : old('name') }}">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Категория</p>
                        <div class="styled-select">
                            <select id="select-main" class="styled @if($errors->first('category_id')) error @endif"
                                    name="category_id">
                                <option disabled selected>Не выбрана:</option>
                                @foreach ($categories as $category)
                                    @if ((isset($pr_cat)) && $pr_cat == $category->id )
                                        <option
                                                @if (isset($pr_cat) && ($category->id == 1 || $category->id == 3)) data-id="0"
                                            @elseif (isset($pr_cat) && ($category->id == 4)) data-id="1"
                                                @endif
                                                value="{{ $category->id }}" selected>{{ $category->title }}</option>
                                    @elseif (old('category_id') == $category->id)
                                        <option value="{{ $category->id }}" selected>{{ $category->title }}</option>
                                    @else
                                        <option
                                                @if (isset($pr_cat) && ($category->id == 1 || $category->id == 3)) data-id="0"
                                                @elseif (isset($pr_cat) && ($category->id == 4)) data-id="1"
                                                @endif
                                                value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endif
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Подкатегория</p>
                        <div class="styled-select sub-select">
                            <select id="select-sub" class="styled" name="sub_category">
                                <option selected value="0">Не выбрана:</option>
                                <option id="empty-sub-cat" value="0" style="display: none" >Нет категорий</option>
                                @foreach ($categories as $category)
                                    @foreach ($category->children as $sub_category)
                                        @if ((isset($pr_subcat)) && $pr_subcat == $sub_category->id )
                                            <option data-id="{{ $sub_category->parent_id }}"
                                                    value="{{ $sub_category->id }}"
                                                    selected>{{ $sub_category->title }}</option>
                                        @elseif (old('sub_category') == $sub_category->id)
                                            <option data-id="{{ $sub_category->parent_id }}"
                                                    value="{{ $sub_category->id }}"
                                                    selected>{{ $sub_category->title }}</option>
                                        {{--@elseif (isset($category->children))--}}
                                        @else
                                            <option data-id="{{ $sub_category->parent_id }}"
                                                    value="{{ $sub_category->id }}">{{ $sub_category->title }}</option>
                                        @endif
                                    @endforeach
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>

                @foreach ($properties as $property)
                    {{--если это Тип Изделия--}}
                    @if ($property->slug == 'type')
                        <div class="col-6 p-0">
                            <div class="input_container">
                                <p class="input_lable">{{ $property->name }}</p>
                                <div class="styled-select">
                                    <select class="styled" id="prop-{{ $property->id }}" name="properties[{{ $property->id }}]">
                                        <option disabled selected>Не выбрана:</option>
                                        @foreach ($property->options as $option)
                                            @if ((isset($product->options[ $property->id - 1]->id)) && $product->options[ $property->id - 1]->id == $option->id)
                                                <option data-id="{{ $option->category_id }}"
                                                        value="{{ $option->id }}" selected>{{ $option->name }}</option>
                                            @elseif (old('properties.' . $property->id) == $option->id)
                                                <option data-id="{{ $option->category_id }}"
                                                        value="{{ $option->id }}" selected>{{ $option->name }}</option>
                                            @else
                                                <option data-id="{{ $option->category_id }}"
                                                        value="{{ $option->id }}">{{ $option->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @else
                    <div class="col-6 p-0">
                        <div class="input_container">
                            <p class="input_lable">{{ $property->name }}</p>
                            <div class="styled-select">
                                <select class="styled" id="prop-{{ $property->id }}" name="properties[{{ $property->id }}]">
                                    <option disabled selected>Не выбрана:</option>
                                    @foreach ($property->options as $option)
                                        @if ((isset($product->options[ $property->id - 1]->id)) && $product->options[ $property->id - 1]->id == $option->id)
                                            <option
                                                    @isset ($option->parent_id) data-id="{{$option->parent_id}}" @endisset
                                                    value="{{ $option->id }}" selected>{{ $option->name }}</option>
                                        @elseif (old('properties.' . $property->id) == $option->id)
                                            <option
                                                    @isset ($option->parent_id) data-id="{{$option->parent_id}}" @endisset
                                                    value="{{ $option->id }}" selected>{{ $option->name }}</option>
                                        @else
                                            <option
                                                    @isset ($option->parent_id) data-id="{{$option->parent_id}}" @endisset
                                                    value="{{ $option->id }}">{{ $option->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach


                <div class="col-6 p-0">
                    <div class="row m-0">
                        <div class="col-6 p-0">
                            <div class="input_container">
                                <p class="input_lable">Наличие</p>
                                <div class="styled-select">
                                    <select class="styled" name="state">
                                        <option value="0"
                                                @if ( isset($product->state) && $product->state == 0) selected="" @endif>
                                            Не в наличии
                                        </option>
                                        <option value="1"
                                                @if ( isset($product->state) && $product->state == 1) selected="" @endif>
                                            В наличии
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-6 p-0">
                            <div class="input_container">
                                <p class="input_lable">Изготовление</p>
                                <input class="@if($errors->first('making')) error @endif" id="day-count" type="text"
                                       placeholder="10 дней" name="making"
                                       value="{{ (isset($product->making)) ? $product->making : old('making') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-9 p-0">
                    <div class="input_container">
                        <p class="input_lable">Краткое описание</p>
                        <input type="text" placeholder="Описание" name="description"
                               value="{{ (isset($product->description)) ? $product->description :  old( 'description') }}">
                    </div>
                </div>
                <div class="col-3 p-0">
                    <div class="input_container">
                        <p class="input_lable">Артикул</p>
                        <input class="@if($errors->first('code')) error @endif" type="text" placeholder="000001"
                               name="code" value={{ (isset($product->code)) ? $product->code :  old( 'code') }}>
                    </div>
                </div>

                <div class="col-12 p-0">
                    <div class="input_container">
                        <p class="input_lable">Полное описание товара</p>
                        <textarea cols="30" rows="5" placeholder="Описание"
                                  name="content">{{ (isset($product->content)) ? $product->content :  old( 'content') }}</textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="btn_container">
            <button class="custom_btn" type="submit">
                <span>@if (Route::current()->getName() == 'admin.product.edit') Изменить товар @else Создать товар@endif</span>
            </button>
        </div>
    </div>
    <div class="product_block col-5 p-0">
        <div class="block_title">
            <h1>Цена, размер, вес</h1>
            <a id="add" href="#"><img src="{{ asset('images/admin/plus - anticon_black.svg') }}" alt=""></a>
        </div>
        <div class="psw_container">
            @php
                       if (isset($product->category_id) && ($product->category_id == 1 || $product->category_id == 3)){
                       $product_sizes = $product_sizes_ring;}
                       elseif (isset($product->category_id) && ($product->category_id == 4)) $product_sizes = $product_sizes_chain;
                       else $product_sizes = [];
            @endphp

            @if ( (!empty(old('price'))) or (!empty(old('weight'))) or (!empty(old('size'))) )
                @php
                    $data['price'] = old('price');
                    $data['weight'] = old('weight');
                    $data['size'] = old('size');

                @endphp
                @foreach ($data['price'] as $key => $item)
                    <div class="input_container">
                        <input id="product-price" name="price[]" type="text" placeholder="150 грн"
                               class="@if (empty($item)) error @endif"
                               value="{{ isset($item) ? $item : '' }}">
                        <input id="product-weight" name="weight[]" type="text" placeholder="15 грамм"
                               class="@if (empty($data['weight'][$key])) error @endif"
                               value="{{ isset($data['weight'][$key]) ? $data['weight'][$key] : ''}}">
                        <div class="styled-select styled-size">
                            <select name="size[]"
                                    class="styled select-size">
                                    {{--class="styled select-size @if (empty($data['size'][$key])) error @endif">--}}
                                <option value="{{ isset($data['size'][$key]) ? $data['size'][$key] : '' }}"
                                        selected>{{ !empty($data['size'][$key]) ? $data['size'][$key] : '' }}</option>
                                @isset ($product_sizes)
                                @foreach ($product_sizes as $key => $size)
                                    <option id="{{$key}}" value="{{ $size }}">{{ $size }}</option>
                                @endforeach
                                @endisset
                            </select>
                        </div>
                        <a href="#" class="delete_psw"><img src="{{asset('images/admin/x_icon.png') }}" alt=""></a>
                    </div>
                @endforeach
            @elseif (isset($product->addoptions))
                @foreach($product->addoptions as $addoption)
                    <div class="input_container">
                        <input id="product-price" name="price[]" type="text" placeholder="150 грн"
                               value="{{ $addoption['price'] }}">
                        <input id="product-weight" name="weight[]" type="text" placeholder="15 грамм"
                               value="{{ $addoption['weight'] }}">
                        <div class="styled-select styled-size">
                            <select name="size[]" class="styled select-size">
                                <option value="{{ $addoption['size'] }}"
                                        selected>{{ $addoption['size'] }}</option>
                                @isset ($product_sizes)
                                @foreach ($product_sizes as $key => $size)
                                    <option id="{{$key}}" value="{{ $size }}">{{ $size }}</option>
                                @endforeach
                                @endisset
                            </select>
                        </div>
                        <a href="#" class="delete_psw"><img src="{{asset('images/admin/x_icon.png') }}" alt=""></a>
                    </div>
                @endforeach
            @endif


        </div>

        {{--@if ($errors->first('weight.*') or $errors->first('price.*') or $errors->first('size'))--}}
        {{--<div class="alert alert-danger">--}}
        {{--<p>@if(!empty($errors->first('price.*'))) Поле цена обязательное для заполнения @endif</p>--}}
        {{--<p>@if($errors->first('weight.*')) Поле вес обязательное для заполнения @endif</p>--}}
        {{--<p>@if($errors->first('size')) Поле размер обязательное для заполнения @endif</p>--}}
        {{--<p>@if(!empty($errors->first('price.*'))) {{ $errors->first('price.*') }} @endif</p>--}}
        {{--<p>@if($errors->first('weight.*')) {{ $errors->first('weight.*') }} @endif</p>--}}
        {{--<p>@if($errors->first('size')) {{ $errors->first('size') }} @endif</p>--}}

        {{--</div>--}}
        {{--@endif--}}

    </div>

</div>
