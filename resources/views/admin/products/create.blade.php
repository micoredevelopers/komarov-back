@extends('admin.layouts.app_admin')

@section('wrap-class')
	<div id="wrapper-product-info">
@endsection

@section('navigation')
	@include('admin.navigation')
@endsection

@section('content')
<main>
	<div class="section_main">
		<div class="container-fluid p-0">
			<div class="row m-0">
				<div class="col-12 p-0">
					<div class="header_search">
						<a href="{{ route('admin.product.index') }}" class="title">
							<img src="{{ asset('images/admin/arrow_left.png') }}" alt="">
							<span>Создание товара</span>
						</a>
					</div>
				</div>
			</div>
			<div class="product_info_container">
				<form id="create-form" action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						@include('admin.products.partials.form')
				</form>



			</div>
		</div>
	</div>
</main>
@endsection