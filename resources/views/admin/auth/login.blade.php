@extends('admin.layouts.app_admin')

@section('content')

<div id="wrapper-login">

		<main>
			<div class="section_login">
				<div class="container p-0">
					<div class="row m-0">
						<div class="col-12 p-0">
							<div class="auth_container">
								<div class="image_container"><img src="{{asset('images/admin/main_logo.png')}}" alt=""></div>
								<form class="input_form" action="{{ route('adminLogin') }}" method="POST">
                                 @csrf
									<div class="input_container">
										<p class="sub_title">Логин*</p>
										<input id="login_form" class="@if ($errors) error  @endif" type="text" placeholder="Логин" value="{{ old('name') }}" name="name">
									</div>
									<div class="input_container">
										<p class="sub_title">Пароль*</p>
										<input id="pass_form" class="@if ($errors) error  @endif" type="password" name="password">
									</div>
									<div class="btn_container">
										<button type="submit" class="btn_custom"><span>Войти</span></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		
	</div>
@endsection
