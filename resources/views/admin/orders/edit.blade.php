@extends('admin.layouts.app_admin')

@section('wrap-class')
    <div id="wrapper-order-info">
@endsection

@section('navigation')
    @include('admin.navigation')
@endsection

@section('content')
    <main>
        <div class="section_main">
            <div class="container-fluid p-0">
                <div class="row m-0">
                    <div class="col-12 p-0">
                        <div class="header_search">
                            <div class="title"><h1>Заказ №{{ $order->id }}</h1></div>
                        </div>
                    </div>
                </div>
                {{--@if ($errors->any())--}}
                    {{--<div class="alert alert-danger">--}}
                        {{--<ul>--}}
                            {{--@foreach ($errors->all() as $error)--}}
                                {{--<li>{{ $error }}</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--@endif--}}
                <div class="order_info_container">
                    <form class="order_form" action="{{route('admin.order.update', $order)}}" method="post">
                        <input type="hidden" name="_method" value="put">
                        {{ csrf_field() }}
                        @include('admin.orders.partials.form')
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection
