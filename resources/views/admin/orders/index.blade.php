@extends('admin.layouts.app_admin')


@section('wrap-class')
	<div id="wrapper-orders">
@endsection

@section('navigation')
	@include('admin.navigation')
@endsection
 
@section('content')

<main>
	<div class="section_main">
		<div class="container-fluid p-0">
			<div class="row m-0">
				<div class="col-12 p-0">
					<div class="header_search">
						<div class="title">
							<h1>Заказы</h1>
						</div>
						<form action="{{ route('adminOrderSearch') }}" method="GET">
							<div class="input_container">
								<button type="submit"><img src="{{ asset('images/admin/search_icon.png') }}" alt=""></button>
								<input id="search" type="text" name="search" placeholder="Поиск">
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="table_main row m-0">
				<div class="col-12 p-0">
					<div class="table_container">
						<table>
							<thead>
							<tr>
								<th><p>Номер заказа</p></th>
								<th><p>Пользователь</p></th>
								<th><p>Сумма</p></th>
								<th><p>Статус</p></th>
								<th><p>Дата</p></th>
								<th><p>Действие</p></th>
							</tr>
							</thead>
							<tbody>
							@forelse($orders as $order)
							<tr>
								<th><p>{{ $order->id }}</p></th>
								<th><p>{{ $order->firstname  }} {{ $order->lastname }}</p></th>
								<th><p>{{ $order->sum }}</p></th>
								<th><p>{{ $order->state }}</p></th>
								<th><p>{{ $order->created_at->format('d.m.Y') }}</p></th>
								<th>
									<div class="order_button">
										<img class="btn_show" src="{{ asset('images/admin/button_detailed_icon.png') }}" alt="">
										<div class="detailed_block">
											<a class="text_btn" href="{{route('admin.order.edit', $order)}}">Редактировать</a>
											<form class="text_btn delete" action="{{route('admin.order.destroy', $order)}}" method="post">
												<input type="hidden" name="_method" value="delete">
												{{ csrf_field() }}
												<button type="submit" class="text_btn" href="{{route('admin.order.destroy', $order)}}">Удалить</button>
											</form>
										</div>
									</div>
								</th>
							</tr>
							@empty
								<tr>
									<td>
										<h3 style="text-alight:center">Данные отсутствуют</h3>
									</td>
								</tr>
							@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>

			@if($orders->lastPage() > 1)
				<div class="pagination_container">

					<div class="page_count">
						<p class="page_numb">{{ $orders->currentPage() }}</p>
						<span>из</span>
						<p class="page_numb">{{ $orders->lastPage() }}</p>
					</div>
					<div class="nav_container">
						{{--для отображения стрелочки "Предыдущая страница"--}}
						@if($orders->currentPage() !== 1)
							<a href="{{ $orders->url(($orders->currentPage() - 1 )) }}"><img src="{{ asset('images/admin/arrow_left.png') }}" alt=""></a>
						@endif
						@if($orders->currentPage() !== $orders->lastPage())
							<a href="{{ $orders->url(($orders->currentPage() + 1 )) }}"><img src="{{ asset('images/admin/arrow_right.png') }}" alt=""></a>
						@endif
					</div>
				</div>
			@endif


		</div>
	</div>
</main>




@endsection