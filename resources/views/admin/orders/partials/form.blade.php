<div class="row m-0">
    <div class="order_block col-6 p-0">
        <div class="form_title">
            <h1>Информация о заказе</h1>
            <h2 class="date">{{ $order->created_at->format('d.m.Y') }}</h2>
        </div>
        <div class="form_order_info">
            <div class="row m-0">
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Имя</p>
                        <input type="text" class="@if($errors->first('firstname')) error @endif" name="firstname" placeholder="Ваше имя"
                               value="{{ isset($order->firstname) ? $order->firstname : '' }}">
                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Фамилия</p>
                        <input type="text" class="@if($errors->first('lastname')) error @endif" name="lastname" placeholder="Ваша фамилия"
                               value="{{ isset($order->lastname) ? $order->lastname : '' }}">
                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Mail</p>
                        <input type="email" class="@if($errors->first('email')) error @endif" name="email" placeholder="Ваш e-mail"
                               value="{{ isset($order->email) ? $order->email : '' }}">
                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Телефон</p>
                        <input type="text" class="@if($errors->first('phone')) error @endif" name="phone" placeholder="Ваш телефон"
                               value="{{ isset($order->phone) ? $order->phone : '' }}">
                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Пол</p>
                        <div class="styled-select">
                            <select class="styled" name="sex" id="styled">
                                <option value="0" @if ( isset($order->sex) && $order->sex == 0) selected="" @endif>
                                    Мужской
                                </option>
                                <option value="1" @if ( isset($order->sex) && $order->sex == 1) selected="" @endif>
                                    Женский
                                </option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Метод доставки</p>
                        <div class="styled-select">
                        <select class="styled" name="delivery" id="styled">
                            <option value="Новая почта" @if ( isset($order->delivery) && $order->delivery == 'Новая почта') selected="" @endif>Новая почта</option>
                            <option value="Курьер" @if ( isset($order->delivery) && $order->delivery == 'Курьер') selected="" @endif>Курьер</option>
                            <option value="Самовывоз" @if ( isset($order->delivery) && $order->delivery == 'Самовывоз') selected="" @endif>Самовывоз</option>
                        </select>
                        </div>
                    </div>

                </div>

                <div class="col-6 p-0">

                    <div class="input_container">
                        <p class="input_lable">Город</p>
                        <div class="styled-select">
                            <select class="styled" name="city" data-order_id="{{ $order->id }}" id="city_id" data-action="{{ route('admin.newpost.store') }}">
                                @foreach($list_city as $city)
                                       <option value="{{ $city->Ref }}" @if ($order->city == $city->DescriptionRu) selected="" @endif>{{ $city->DescriptionRu }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col-6 p-0">

                    <div class="input_container">
                        <p class="input_lable">Адрес доставки</p>
                        <div class="styled-select">
                            <select class="styled" name="address" id="address_id">
                                @isset ($order->address) <option id="" value="{{ $order->address }}" selected="">{{ $order->address }}</option> @endisset
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12 p-0">
                    <div class="input_container">
                        <p class="input_lable">Комментарий к доставке</p>
                        <textarea name="description" cols="30" rows="5" placeholder="Ваш комментарий">
                            {{ isset($order->description) ? $order->description : '' }}
                        </textarea>
                    </div>
                </div>
                <div class="col-6 p-0">
                    <div class="input_container">
                        <p class="input_lable">Статус заказа</p>
                        <div class="styled-select">
                            <select class="styled" name="state" id="styled">
                                <option value="Новый" @if ( isset($order->state) && $order->state == 'Новый') selected="" @endif>Новый</option>
                                <option value="Принят в обработку" @if ( isset($order->state) && $order->state == 'Принят в обработку') selected="" @endif>Принят в работу</option>
                                <option value="Оплачен" @if ( isset($order->state) && $order->state == 'Оплачен') selected="" @endif>Оплачен</option>
                                <option value="Отправлен" @if ( isset($order->state) && $order->state == 'Отправлен') selected="" @endif>Отправлен</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="btn_container">
            <button class="custom_btn save" type="submit"><span>Сохранить</span></button>
            <button class="custom_btn"><a href="{{ route('admin.order.index') }}">Отменить</a></button>
        </div>
    </div>

    <div class="order_block col-6 p-0">
        <div class="order_list">
            <div class="form_title">
                <h1>Информация о заказе</h1>
            </div>
            <div class="list_container">

                @foreach ($products as $product)
                    {{--@php $product->product->images = json_decode($product->product->images);--}}
                    {{--@endphp--}}
                    <div class="parent_product_info">
                        <div class="product_info">
                            <div class="product_img_name">
                                <div class="image_container"><img
                                            src="{{ Storage::disk('order')->url($product->image ) }}"
                                            alt=""></div>
                                <div class="product_name"><p><span>{{ $product->name }}</span>, размер
                                        <span>{{ $product->size }}</span>, вес <span>{{ $product->weight }}</span>г.,
                                        к-во <span>{{ $product->count }}</span></p>
                                </div>
                            </div>
                            <div class="product_price">
                                <div class="text"><p><span>{{ $product->price * $product->count }}</span> грн</p></div>
                                {{--<div class="text"><p><span>{{ rtrim($product->price, '0') }}</span> грн</p></div>--}}
                                <div class="btn_container">
                                    <a href="#">Удалить</a>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="del_prod" name="del_prod[]" value="">
                        <input type="hidden" name="id_addoption[]" value="{{ $product->id }}">
                    </div>
                @endforeach

            </div>
            <div class="sum_all">
                <div class="text"><p>Общая сумма:</p></div>
                <div class="text"><p class="sum"><span>0</span> грн</p></div>
                <input type="hidden" name="sum" value="">
            </div>
        </div>
    </div>
</div>