@extends('admin.layouts.app_admin')


@section('wrap-class')
	<div id="wrapper-reviews">
@endsection

@section('navigation')
	@include('admin.navigation')
@endsection
 
@section('content')


	<main>
		<div class="section_main">
			<div class="container-fluid p-0">
				<div class="row m-0">
					<div class="col-12 p-0">
						<div class="header_search">
							<div class="title">
								<h1>Отзывы</h1>
							</div>
							<form action="{{ route('adminCommentSearch') }}" method="GET">
								<div class="input_container">
									<button type="submit"><img src="{{ asset('images/admin/search_icon.png') }}" alt=""></button>
									<input id="search" type="text" name="search" placeholder="Поиск">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="table_main row m-0">
					<div class="col-12 p-0">
						<div class="table_container">
							<table>
								<thead>
								<tr>
									<th>
										<p>Отзыв</p>
									</th>
									<th>
										<p>ФИО</p>
									</th>
									<th>
										<p>Статус</p>
									</th>
									<th>
										<p>Товар</p>
									</th>
									<th>
										<p>Действие</p>
									</th>
								</tr>
								</thead>
								<tbody>
								@forelse($comments as $comment)
								<tr>
									<th>
										<p>{{ isset($comment->text) ? $comment->text : ''}}</p>
									</th>
									<th>
										<p>
											{{ isset($comment->user->user_additional->firstname) ? $comment->user->user_additional->firstname : ''}}
											{{ isset($comment->user->user_additional->lastname) ? $comment->user->user_additional->lastname : ''}}
										</p>
									</th>
									<th>
										<p>{{ isset($comment->status) ? $comment->status : ''}}</p>
									</th>
									<th>
										<p>{{ isset($comment->product->name) ? $comment->product->name : ''}}</p>
									</th>
									<th>

										<div class="order_button">
											<img class="btn_show" src="{{ asset('images/admin/button_detailed_icon.png') }}" alt="">
											<div class=" detailed_block">
												<a class="text_btn" href="{{route('admin.comment.access', $comment)}}">Разрешить</a>
												<a class="text_btn" href="{{route('admin.comment.block', $comment)}}">Запретить</a>
											</div>
										</div>
									</th>
								</tr>
								@empty
									<tr>
										<td>
											<h3 style="text-alight:center">Данные отсутствуют</h3>
										</td>
									</tr>
								@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
				@if($comments->lastPage() > 1)
					<div class="pagination_container">

						<div class="page_count">
							<p class="page_numb">{{ $comments->currentPage() }}</p>
							<span>из</span>
							<p class="page_numb">{{ $comments->lastPage() }}</p>
						</div>
						<div class="nav_container">
							@if($comments->currentPage() !== 1)
								<a href="{{ $comments->url(($comments->currentPage() - 1 )) }}"><img src="{{ asset('images/admin/arrow_left.png') }}" alt=""></a>
							@endif
							@if($comments->currentPage() !== $comments->lastPage())
								<a href="{{ $comments->url(($comments->currentPage() + 1 )) }}"><img src="{{ asset('images/admin/arrow_right.png') }}" alt=""></a>
							@endif
						</div>
					</div>
				@endif
			</div>
		</div>
	</main>
@endsection