	<header>
			<div class="container-fluid p-0">
				<div class="row m-0">
					<div class="col-12 p-0">
						<div class="nav_container">
							<div class="logo_container"><img src="{{ asset('images/admin/main_logo_white.svg') }}" alt=""></div>
							<ul class="nav_menu">
								<li class="nav_item">
									<a href="{{ route('admin.product.index') }}">
										<div class="img_container"><img src="{{ asset('images/admin/goods_icon.png') }}" alt=""></div>
										<div class="text"><p>Товары</p></div>
									</a>
								</li>
								<li class="nav_item">
									<a href="{{ route('admin.order.index') }}">
										<div class="img_container"><img src="{{ asset('images/admin/orders_icon.png') }}" alt=""></div>
										<div class="text"><p>Заказы</p></div>
									</a>
								</li>
								<li class="nav_item">
									<a href="{{ route('admin.user.index') }}">
										<div class="img_container"><img src="{{ asset('images/admin/users_icon.png') }}" alt=""></div>
										<div class="text"><p>Пользователи</p></div>
									</a>
								</li>
								<li class="nav_item">
									<a href="{{ route('admin.comment.index') }}">
										<div class="img_container"><img src="{{ asset('images/admin/rating.svg') }}" alt=""></div>
										<div class="text"><p>Отзывы</p></div>
									</a>
								</li>
								<li class="nav_item">
									<a href="{{ route('adminSetting') }}">
										<div class="img_container"><img src="{{ asset('images/admin/settings_icon.png') }}" alt=""></div>
										<div class="text"><p>Настройки</p></div>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
