@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="flex-center position-ref full-height">
                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <a href="{{ url('/home') }}">Home</a>
                                    @else
                                        <a href="{{ route('login') }}">Login</a>
                                        <a href="{{ route('register') }}">Register</a>
                                    @endauth
                                </div>
                            @endif

                            <div class="content">
                                <div class="title m-b-md">
                                    Laravel
                                </div>

                                <div class="links">
                                    <a href="https://laravel.com/docs">Documentation</a>
                                    <a href="https://laracasts.com">Laracasts</a>
                                    <a href="https://laravel-news.com">News</a>
                                    <a href="https://forge.laravel.com">Forge</a>
                                    <a href="https://github.com/laravel/laravel">GitHub</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


