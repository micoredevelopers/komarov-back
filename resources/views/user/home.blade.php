@extends('layouts.app')


@section('title', 'Komarov | Home')
@section('meta_keyword', '')
@section('meta_description', '')


@section('wrap-class')
    <div id="wrapper-main">
@endsection


@section('content')
    <main>
        <section class="section_slide">
            <div class="bg_overlay"></div>
            <div class="main-carousel">
                <div class="carousel-cell">
                    <div class="image_container">
                        <img src="{{ asset('images/main/images/main/main-slide-image.png') }}" alt="">
                    </div>
                    <div class="slide_text">
                        <h3 class="sub_title">gold and silver</h3>
                        <h1 class="title">Shop the new collection with free shipping</h1>
                    </div>
                </div>
                <div class="carousel-cell">
                    <div class="image_container">
                        <img src="{{ asset('images/main/images/main/main-slide-image.png') }}" alt="">
                    </div>
                    <div class="slide_text">
                        <h3 class="sub_title">gold and silver</h3>
                        <h1 class="title">Shop the new collection with free shipping</h1>
                    </div>
                </div>
                <div class="carousel-cell">
                    <div class="image_container">
                        <img src="{{ asset('images/main/images/main/main-slide-image.png') }}" alt="">
                    </div>
                    <div class="slide_text">
                        <h3 class="sub_title">gold and silver</h3>
                        <h1 class="title">Shop the new collection with free shipping</h1>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection







