<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('firstname');
            $table->string('lastname');
            $table->text('description')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->bolean('sex');
            $table->string('city');
            $table->string('delivery');
            $table->string('address');
            $table->double('sum');
            $table->string('state');
        
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
