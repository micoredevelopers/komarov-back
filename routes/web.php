<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Route::group(['prefix' => ''], function () {
    Auth::routes();
    Route::get('/', 'IndexController@index')->name('index');
    Route::get('/home', 'User\HomeController@index')->name('home');
//Для отображения категорий и новостей
    Route::get('/category/{slug}', 'IndexController@category')->name('category');
    Route::get('/category/{slug}/{filter?}', 'IndexController@category')->name('filter');



    Route::get('/product/{id?}', 'IndexController@product')->name('product');
//    Route::get('/filter', 'IndexController@filterReset')->name('filterReset');
//    Route::post('/filter', 'IndexController@filter')->name('filter');

});

//Авторизация для админа
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/login', 'Auth\MyAuthController@showLogin')->name('adminLogin');
    Route::post('/login', 'Auth\MyAuthController@authenticate');
    Route::post('/logout', 'Auth\MyAuthController@logout')->name('adminLogout');
});

//Посредник father смотрит авторизовался ли админ
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'father'], function () {

    Route::get('/', 'AdminController@store')->name('adminStore');
    //смена пароля
    Route::get('/setting', 'AdminController@index')->name('adminSetting');
    Route::post('/setting', 'AdminController@update')->name('adminSettingUpdate');
    //Товары
    Route::resource('/product', 'ProductController', ['as' => 'admin']);
    Route::get('/product/{name?}', 'ProductController@index', ['as' => 'admin'])->name('adminSearch');
    Route::get('/product/{product}/absent', 'ProductController@absent', ['as' => 'admin'])->name('adminAbsent');
    //Заказы
    Route::resource('/order', 'OrderController', ['as' => 'admin']);
    Route::get('/order/{name?}', 'OrderController@index', ['as' => 'admin'])->name('adminOrderSearch');
    //Новая почта
    Route::resource('newpost', 'NewpostController', ['as' => 'admin', 'only' => ['store']]);
    //Пользователи
    Route::resource('/user', 'UserController', ['as' => 'admin']);
    Route::get('/user/{user?}', 'UserController@index', ['as' => 'admin'])->name('adminUserSearch');
    Route::get('/user/{user}/block', 'UserController@block', ['as' => 'admin'])->name('adminUserBlock');
    //Комментарии
    Route::get('/comment', 'CommentController@index', ['as' => 'admin'])->name('admin.comment.index');
    Route::get('/comment/{id?}', 'CommentController@index', ['as' => 'admin'])->name('adminCommentSearch');
    Route::get('/comment/{comment}/block', 'CommentController@block', ['as' => 'admin'])->name('admin.comment.block');
    Route::get('/comment/{comment}/access', 'CommentController@access', ['as' => 'admin'])->name('admin.comment.access');
});
